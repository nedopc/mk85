#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ADD 169093

int main(int argc, char** argv)
{
 FILE *f,*fo;
 int i,j;
 char str[256],*po;


 if(argc<2) return -1;
 fo = fopen("fixdrill.out","wt");
 if(fo==NULL) return -2;
 f = fopen(argv[1],"rt");
 if(f==NULL){fclose(fo);return -3;}
 while(1)
 {
   fgets(str,256,f);
   if(feof(f)) break;
   po = strrchr(str,'\n');
   if(po!=NULL) *po = 0;
   if(str[1]=='-')
   {
     i = atoi(&str[1]);
     j = i + ADD;
     printf("%s -> %i -> %06d\n",str,i,j);
     po = strchr(str,'Y');
     if(po==NULL) printf("ERROR!\n");
     else fprintf(fo,"X%06d%s\n",j,po);
   }
   else fprintf(fo,"%s\n",str);
 }
 fclose(f);
 fclose(fo);

 return 0;
}
