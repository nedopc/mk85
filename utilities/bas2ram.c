/* The program converts a list of BASIC programs to the MK-85 memory image.
   It's annotated for the "Splint" checking tool which is available for free
   at http://www.splint.org/	*/

#include <stdio.h>
#include <ctype.h>	/* islower, toupper, isdigit, iscntrl, isspace */
#include "bool.h"

typedef unsigned int uint;
typedef unsigned char uchar;

/* structure for the keyword table tree */
typedef struct treestr
{
  char *tree_str;
  char tree_val;
  /*@null@*/ struct treestr *left;
  /*@null@*/ struct treestr *right;
} TREE;


TREE keywords[] = {
  { "<=",	'\137',	NULL	,	NULL		},	/* index 0 */
  { "<>",	'\134',	keywords+0,	keywords+2	},	/* index 1 */
  { "=<",	'\137',	NULL,		NULL		},	/* index 2 */
  { "=>",	'\176',	keywords+1,	keywords+5	},	/* index 3 */
  { ">=",	'\176',	NULL,		NULL		},	/* index 4 */
  { "ABS",	'\312',	keywords+4,	keywords+6	},	/* index 5 */
  { "ACS",	'\304',	NULL,		NULL		},	/* index 6 */

  { "ASCI",	'\321',	keywords+3,	keywords+11	},	/* index 7 */

  { "ASN",	'\303',	NULL,		NULL		},	/* index 8 */
  { "ATN",	'\305',	keywords+8,	keywords+10	},	/* index 9 */
  { "AUTO",	'\356',	NULL,		NULL		},	/* index 10 */
  { "CHR",	'\320',	keywords+9,	keywords+13	},	/* index 11 */
  { "CLEAR",	'\357',	NULL,		NULL		},	/* index 12 */
  { "COS",	'\301',	keywords+12,	keywords+14	},	/* index 13 */
  { "CSR",	'\327',	NULL,		NULL		},	/* index 14 */


  { "DEFM",	'\346',	keywords+7,	keywords+23	},	/* index 15 */


  { "DRAW",	'\353',	NULL,		NULL		},	/* index 16 */
  { "DRAWC",	'\352',	keywords+16,	keywords+18	},	/* index 17 */
  { "END",	'\344',	NULL,		NULL		},	/* index 18 */
  { "EXP",	'\310',	keywords+17,	keywords+21	},	/* index 19 */
  { "FOR",	'\335',	NULL,		NULL		},	/* index 20 */
  { "FRAC",	'\315',	keywords+20,	keywords+22	},	/* index 21 */
  { "GETC",	'\324',	NULL,		NULL		},	/* index 22 */

  { "GOSUB",	'\332',	keywords+19,	keywords+27	},	/* index 23 */

  { "GOTO",	'\331',	NULL,		NULL		},	/* index 24 */
  { "IF",	'\334',	keywords+24,	keywords+26	},	/* index 25 */
  { "INPUT",	'\337',	NULL,		NULL		},	/* index 26 */
  { "INT",	'\313',	keywords+25,	keywords+29	},	/* index 27 */
  { "KEY",	'\326',	NULL,		NULL		},	/* index 28 */
  { "LEN",	'\317',	keywords+28,	keywords+30	},	/* index 29 */
  { "LETC",	'\345',	NULL,		NULL		},	/* index 30 */


  { "LIST",	'\355',	keywords+15,	keywords+47	},	/* index 31 - ROOT */


  { "LN",	'\307',	NULL,		NULL		},	/* index 32 */
  { "LOG",	'\306',	keywords+32,	keywords+34	},	/* index 33 */
  { "MID",	'\323',	NULL,		NULL		},	/* index 34 */
  { "MODE",	'\350',	keywords+33,	keywords+37	},	/* index 35 */
  { "NEXT",	'\330',	NULL,		NULL		},	/* index 36 */
  { "PI",	'\174',	keywords+36,	keywords+38	},	/* index 37 */
  { "PRINT",	'\336',	NULL,		NULL		},	/* index 38 */

  { "RAN#",	'\325',	keywords+35,	keywords+43	},	/* index 39 */

  { "RETURN",	'\333',	NULL,		NULL		},	/* index 40 */
  { "RND",	'\322',	keywords+40,	keywords+42	},	/* index 41 */
  { "RUN",	'\354',	NULL,		NULL		},	/* index 42 */
  { "SET",	'\351',	keywords+41,	keywords+45	},	/* index 43 */
  { "SGN",	'\314',	NULL,		NULL		},	/* index 44 */
  { "SIN",	'\300',	keywords+44,	keywords+46	},	/* index 45 */
  { "SQR",	'\311',	NULL,		NULL		},	/* index 46 */


  { "STEP",	'\342',	keywords+39,	keywords+55	},	/* index 47 */


  { "STOP",	'\343',	NULL,		NULL		},	/* index 48 */
  { "TAN",	'\302',	keywords+48,	keywords+50	},	/* index 49 */
  { "TEST",	'\360',	NULL,		NULL		},	/* index 50 */
  { "THEN",	'\340',	keywords+49,	keywords+53	},	/* index 51 */
  { "TO",	'\341',	NULL,		NULL		},	/* index 52 */
  { "VAC",	'\347',	keywords+52,	keywords+54	},	/* index 53 */
  { "VAL",	'\316',	NULL,		NULL		},	/* index 54 */

  { "WHO",	'\361',	keywords+51,	NULL		}	/* index 55 */
};

#define KEYWORDS_ROOT (keywords+31)


/* Cyrillic character conversion table Windows-1251 -> MK85, codes 0xC0-0xFF.
   Characters "jo" aren't included and have to be handled separately. */
char chartab[] = {
  '\241', '\242', '\267', '\247', '\244', '\245', '\266', '\272',
  '\251', '\252', '\253', '\254', '\255', '\256', '\257', '\260',
  '\262', '\263', '\264', '\265', '\266', '\250', '\243', '\276',
  '\273', '\275', '\036', '\271', '\270', '\274', '\240', '\261',
  '\201', '\202', '\227', '\207', '\204', '\205', '\226', '\232',
  '\211', '\212', '\213', '\214', '\215', '\216', '\217', '\220',
  '\222', '\223', '\224', '\225', '\206', '\210', '\203', '\236',
  '\233', '\235', '\037', '\231', '\230', '\234', '\200', '\221'
};


#define MAXINPUTLINE 256
char input_line[MAXINPUTLINE];	/* buffer for the input BASIC line */
char *input_ptr;		/* pointer to the input_line buffer */

#define RAMSTART 0x8000			/* address of the RAM */
#define VARS 26				/* number of variables */
#define OUTBUFSIZE 0x1800
#define MAXOUTBUF (OUTBUFSIZE - VARS*8)
char output_buf[OUTBUFSIZE];		/* buffer for the output code */
char *output_ptr;			/* pointer to the output_buf buffer */


/* returns the MK-85 code of a Windows-1251 character */
char convchar (char c)
{
  if (c == '\250')	/* JO */
  {
    return '\277';
  }
  if (c == '\270')	/* jo */
  {
    return '\237';
  }
  if ((uchar) c >= (uchar) '\300')
  {
    return chartab[(int) ((uchar) c - (uchar) '\300')];
  }
  return c;
}


/* This function compares two strings without case sensitivity (first string
   pointed to by ptr2, second string pointed to by input_ptr), and updates the
   input_ptr when both string equivalent. */
int cmp_string (char *ptr2)
{
  char c1, c2;
  char *ptr1 = input_ptr;

  do {
    c1 = *ptr1++;
    if (islower((int) (uint) (uchar) c1) != 0)
    {
      c1 = toupper(c1);
    }
    c2 = *ptr2++;
  } while (c1==c2 && c2!='\0');
  if (c2=='\0')
  {
    input_ptr = --ptr1;
  }
  return (c2=='\0') ? 0 : (int) (c1-c2);
}


/* This function searches the table of structure TREE for a string pointed to
   by the input_ptr, and picks from the table the longest matching string.
   Returns pointer to the table and updated input_ptr when string found,
   or returns NULL and leaves input_ptr unchanged when not found.
   [Splint] The function creates a new pointer to an existing object pointed
   to by root, thus the returned value is annotated as "dependent".	*/
/*@dependent@*//*@null@*/ TREE *find_tree (TREE *root)
{
  TREE *treeptr=NULL;
  char *ptr1=input_ptr;
  char *maxptr=input_ptr;
  int compare;

  while (root != NULL)
  {
    input_ptr=ptr1;
    if ((compare = cmp_string(root->tree_str)) == 0)
    {
      treeptr=root;
      maxptr=input_ptr;
    }
    root = (compare<0) ? root->left : root->right;
  }
  input_ptr = (treeptr != NULL) ? maxptr : ptr1;
  return treeptr;
}


void put_word (uint x)
{
  *output_ptr++ = (char) (x % 256);
  *output_ptr++ = (char) (x / 256);
}


/* returns TRUE when input_line successfully parsed */
bool parse (void)
{
  uint line_number;
  TREE *treeptr1;
  char *max_out_ptr;
  char *save_ptr;
  bool comment = FALSE;
  bool quotes = FALSE;
  bool digit = FALSE;

  max_out_ptr = output_ptr+63;

/* line number */
  save_ptr = input_ptr;
  while (isdigit((int) (uint) (uchar) *input_ptr) != 0)
  {
    input_ptr++;
  }
  if (input_ptr == save_ptr)
  {
    return FALSE;		/* missing line number */
  }
  (void) sscanf (save_ptr, "%u", &line_number);
  put_word (line_number);

  while (*input_ptr!='\0' && output_ptr<max_out_ptr)
  {

/* quotation mark ? */
    if (*input_ptr == '"')
    {
      quotes = !quotes;
    }

/* characters within quotes and after an exclamation mark copied literally */
    if (quotes || comment)
    {
      if (iscntrl((int) (uint) (uchar) *input_ptr) == 0)
      {
        *output_ptr++ = convchar (*input_ptr);
      }
      input_ptr++;
      digit = FALSE;
    }

/* process characters outside quotes and comments */
    else
    {

/* an attempt to tokenize */
      if ((treeptr1 = find_tree(KEYWORDS_ROOT)) != NULL)
      {
        *output_ptr++ = treeptr1->tree_val;
        digit = FALSE;
      }

/* exclamation mark? */
      else if (*input_ptr == '!')
      {
        *output_ptr++ = '!';
        input_ptr++;
        comment = TRUE;
        digit = FALSE;
      }

/* exponent? */
      else if (digit && *input_ptr=='E')
      {
        input_ptr++;
        if (*input_ptr=='-')
        {
          *output_ptr++ = '\175';
          input_ptr++;
        }
        else
        {
          *output_ptr++ = '\173';
        }
        digit = FALSE;
      }

/* skip blanks */
      else if (isspace((int) (uint) (uchar) *input_ptr) != 0)
      {
        input_ptr++;
      }

/* other characters copied literally */
      else
      {
        *output_ptr++ = convchar (*input_ptr);
        digit = (isdigit((int) (uint) (uchar) *input_ptr) != 0);
        input_ptr++;
      }
    }

  }

  *output_ptr++ = '\0';
  return (*input_ptr == '\0');
}


int main (int argc, char *argv[])
{
  FILE *fp;
  uint i;
  char *save_ptr;
  uint errors = 0;
  uint line_counter = 0;
  uint program;

/* clear the output_buf */
  for (i=0; i<OUTBUFSIZE; i++)
    output_buf[i] = '\0';

/* initialise the program address table */
  output_ptr = output_buf+0x022C;
  for (i=0; i<10; i++)
  {
    put_word (0x826B);
  }

/* process the BASIC programs */
  output_ptr = output_buf+0x026B;
  for (program=0; program<10; program++)
  {
    if (--argc <= 0)
      break;
    if ((fp = fopen(*++argv, "rt")) != NULL)
    {
      while (fgets(input_line, MAXINPUTLINE, fp)!=NULL && *input_line!='\032')
      {
        input_ptr = input_line;
        line_counter++;
  /* skip leading spaces */
        while (isspace((int) (uint) (uchar) *input_ptr) != 0)
        {
          input_ptr++;
        }
  /* an empty line? */
        if (*input_ptr == '\0')
        {
          continue;
        }
  /* BASIC line */
        if (!parse())
        {
          fprintf (stderr, "\nError(s) found in line %u", line_counter);
          errors++;
        }
  /* update the program address table */
        save_ptr = output_ptr;		/* save the output_ptr */
        output_ptr = output_buf + 0x022C + 2*program;
        for (i=program; i<10; i++)
        {
          put_word (save_ptr - output_buf + RAMSTART);
        }
        output_ptr = save_ptr;		/* restore the output_ptr */
  /* test for the size of the output code */
        if (output_ptr > output_buf+MAXOUTBUF)
        {
          fprintf (stderr, "\nExceeded maximal program size");
          errors++;
          program = 10;		/* to break the external loop as well */
          break;
        }
      }
      (void) fclose (fp);
    }
    else
    {
      fprintf (stderr, "\nCannot open the file %s\n",*argv);
    }
  }

  fprintf (stderr, "\nErrors: %u\n", errors);

/* determine the required RAM size */
  i = output_ptr - output_buf;
  if (i <= 0x0800-VARS*8)
    i = 0x0800;			/* MK-85 */
  else
    i = 0x1800;			/* MK-85M */

/* initialise important system variables */
  output_ptr = output_buf+0x0250;
  put_word (VARS);		/* number of variables */
  put_word (RAMSTART+i);	/* top of the RAM */

/* save the output file */
  if ((fp = fopen("RAM.BIN", "wb")) == NULL)
  {
    fprintf (stderr, "\nCannot open output file RAM.BIN\n");
    return 1;
  }
  if (fwrite (output_buf, sizeof (char), (size_t) i, fp) != (size_t) i)
  {
    fprintf (stderr, "\nFile write error, disk full?\n");
    (void) fclose (fp);
    return 1;
  }
  (void) fclose (fp);

  return 0;
}
