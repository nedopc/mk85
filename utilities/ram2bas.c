/* This utility lists the BASIC programs from the MK-85 memory image */

#include <stdio.h>

typedef unsigned int uint;


/* character codes 0x00 to 0xBF */
const char characters[] =
"???????{}?_o???_"
"x:??????v<>??.\332\372"
" !\042#$%&'()*+,-./"
"0123456789:;<=>?"
"@ABCDEFGHIJKLMNO"
"PQRSTUVWXYZ[?]^?"
" abcdefghijklmno"
"pqrstuvwxyzE????"
"\376\340\341\366\344\345\364\343\365\350\351\352\353\354\355\356"
"\357\377\360\361\362\363\346\342\374\373\347\370\375\371\367\270"
"\336\300\301\326\304\305\324\303\325\310\311\312\313\314\315\316"
"\317\337\320\321\322\323\306\302\334\333\307\330\335\331\327\250";


/* BASIC keywords 0xC0 to 0xF1 */
const char *tokens[] = {
	"SIN ",		"COS ",		"TAN ",		"ASN ",
	"ACS ",		"ATN ",		"LOG ",		"LN ",
	"EXP ",		"SQR ",		"ABS ",		"INT ",
	"SGN ",		"FRAC ",	"VAL ",		"LEN ",
	"CHR ",		"ASCI ",	"RND ",		"MID ",
	"GETC ",	"RAN#",		"KEY",		"CSR ",
	"NEXT ",	"GOTO ",	"GOSUB ",	"RETURN",
	"IF ",		"FOR ",		"PRINT ",	"INPUT ",
	" THEN ",	" TO ",		" STEP ",	"STOP",
	"END",		"LETC ",	"DEFM ",	"VAC",
	"MODE ",	"SET ",		"DRAWC ",	"DRAW ",
	"RUN ",		"LIST ",	"AUTO ",	"CLEAR ",
	"TEST",		"WHO"
};


uint addrtable[10];


int main(int argc, char *argv[])
{
  int x1, x2;
  uint i1, i2, program;
  FILE *fp;

  if (argc<=1)
  {
    fprintf (stderr, "\nMissing file name, program aborted\n");
    return 1;
  }

  if ((fp = fopen(*++argv, "rb")) == NULL)
  {
    fprintf (stderr, "\nCannot open the file %s\n", *argv);
    return 1;
  }

/* read the address table which starts at address 0x822C */
  (void) fseek (fp, (long) 0x022C, SEEK_SET);
  i2 = (uint) 0x826B;
  for (i1=0; i1<10; i1++)
  {
    if ((x1 = getc(fp)) == EOF || (x2 = getc(fp)) == EOF)
    {
      (void) fclose (fp);
      fprintf (stderr, "\nPremature end of the file\n");
      return 1;
    }
/* check the addresses for monotonity and range */
    if ((addrtable[i1] = (uint) x1 + (((uint) x2) << 8)) < i2)
    {
      (void) fclose (fp);
      fprintf (stderr, "\nInvalid data\n");
      return 1;
    }
    i2 = addrtable[i1];
  }


/* read the BASIC programs which start at address 0x826B */
  (void) fseek (fp, (long) 0x002B, SEEK_CUR);
  i2 = 0;		/* program state: first byte of the line number */
  i1 = (uint) 0x826B;
  for (program=0; program<10; program++)
  {
    printf ("\nP%u\n", program);
    while (i1 < addrtable[program])
    {
      x2 = x1;
      if ((x1 = getc(fp)) == EOF)
      {
        (void) fclose (fp);
        fprintf (stderr, "\nPremature end of the file\n");
        return 1;
      }
      switch (i2)
      {
        case 0:		/* first byte of the line number */
          i2++;
          break;
        case 1:		/* second byte of the line number */
          printf ("%u ", (((uint) x1) << 8) + (uint) x2);
          i2++;
          break;
        default:
          if (x1 == 0)
          {
            printf ("\n");
            i2 = 0;	/* line number expected */
          }
          else if (x1 == 0x5C)
          {
            printf ("<>");
          }
          else if (x1 == 0x5F)
          {
            printf ("<=");        
          }
          else if (x1 == 0x7C)
          {
            printf ("PI");
          }
          else if (x1 == 0x7D)
          {
            printf ("E-");
          }
          else if (x1 == 0x7E)
          {
            printf (">=");
          }
          else if (x1 < 0xC0)
          {
            printf ("%c", characters[x1]);
          }
          else if (x1 < 0xF2)
          {
            printf ("%s", tokens[x1 - 0xC0]);
          }
          else
          {
            printf ("{%02X}", (uint) x1);
          }
          break;
      }
      i1++;
    }
  }

  (void) fclose (fp);
  return 0;
}
