/* This utility lists all non-zero variables from the MK-85 memory image */

#include <stdio.h>
#include "typedefs.h"


/* character codes 0x00 to 0xBF, tilde marks unprintable characters */
const char characters[] =
"~~~~~~~{}~~~~~~_"
"~~~~~~~~~~~~~~\332\372"
" !~#$%&'()*+,-./"
"0123456789:;<=>?"
"@ABCDEFGHIJKLMNO"
"PQRSTUVWXYZ[~]^~"
" abcdefghijklmno"
"pqrstuvwxyz~~~~~"
"\376\340\341\366\344\345\364\343\365\350\351\352\353\354\355\356"
"\357\377\360\361\362\363\346\342\374\373\347\370\375\371\367\270"
"\336\300\301\326\304\305\324\303\325\310\311\312\313\314\315\316"
"\317\337\320\321\322\323\306\302\334\333\307\330\335\331\327\250";


uchar inbuf[30];
char outbuf[256];


#define RAMSTART 0x8000			/* address of the RAM */


uint get_word (uchar *ptr)
{
  return (uint) *ptr + (((uint) *(ptr+1)) << 8);
}


/* The procedure converts an FP number to a string. It implements the original
 algorithm from the MK-85 ROM, found at address 28CC. Unfortunately, I wasn't
 able to disentangle it to a more clean, structured code. */
void fp2str (uchar *fpnumber, int spec_precision, char *destination)
{
  int sign;
  int exponent;
  char buffer[16];

  char *bufptr;		/* r5 in original procedure */
  char *destptr;	/* r4 in original procedure */
  int act_precision;	/* r4 in original procedure */
  int i;

  sign = 0;
  bufptr = buffer;
  *bufptr++ = '0';

  exponent = (int) get_word (fpnumber);
  sign = 0;
  if (exponent != 0)
  {
    sign = exponent & 0x8000;
    exponent &= 0x7FFF;
  }
  exponent = (int) ((uint) exponent + 0xF000);

  act_precision = -spec_precision;
  if (spec_precision >= 0 || act_precision > 10)
  {
    act_precision = 10;		/* default printing precision */
  }

/* convert act_precision+1 digits of the mantissa to ASCII,
   done differently than in the original code, but gives the same result */
  for (i=0; i<=act_precision; i++)
  {
    *bufptr++ = characters[48 +
	((get_word(fpnumber + (i/4+1)*2) >> (12 - (i%4)*4)) & 0x000F)];
  }

/* rounding (the last digit will be dropped) */
  if (*--bufptr >= '5')
  {
    while (++(*--bufptr) > '9')
      ;
    bufptr++;
  }

/* remove trailing zeros */
  do {
    *bufptr = '\0';
  } while (*--bufptr == '0' && bufptr > buffer);

/* remove the leading zero */
  bufptr = buffer;
  if (*bufptr++ != '0')
  {
    bufptr--;
    exponent++;
  }

/* output the minus sign for negative numbers */
  destptr = destination;
  *destptr++ = '-';
  if (sign >= 0)
  {
/* space before a positive number, unless maximal precision set */
    *--destptr = ' ';
    if (spec_precision < 0)
    {
      destptr++;
    }
  }

  i = exponent;		/* i corresponds to r2 */
  if (i > 0)
    goto l29aa;
  if (i == -0x1000)	/* input number = 0 */
    goto l29ae;
  i = -i;
  if (i > 2)
    goto l29b4;

  if (spec_precision >= 0)
  {
    i -= act_precision;
    if (destptr != destination)
    {
      act_precision--;
    }
  }

/* fractional number in normal (not scientific) display format */
  *destptr++ = '0';
  *destptr++ = '.';
/* zeros between the decimal point and the mantissa */
  while (--i >= 0)
  {
    *destptr++ = '0';
  }

l29aa:
  if (i <= act_precision)
  {
l29ae:
    exponent = 0;
    goto l29de;
  }

l29b4:
  if (spec_precision >= 0)
  {
    act_precision = 3;
    if (destptr == destination)
    {
      act_precision++;
    }
/* loop from address 29C6 unrolled */
    if (i < 1000)
    {
      act_precision++;
      if (i < 100)
      {
        act_precision++;
        if (i < 10)
        {
          act_precision++;
        }
      }
    }
  }

  exponent--;
  i = 1;	/* i equivalent to r2 */

l29de:
/* 29DE: this loop outputs act_precision digits of the the mantissa,
   i = number of digits before the decimal point */
  while (act_precision-- != 0)
  {
    *destptr++ = *bufptr++;
    if (*bufptr == '\0')
    {
/* 29F0: this loop outputs trailing zeros */
      while (--i > 0)
      {
        *destptr++ = '0';
      }
      break;
    }
    if (--i == 0)
    {
      *destptr++ = '.';
    }
  }

/* 29F8: output the exponent */
  if (exponent != 0)
  {
/* in C there's no need to treat positive and negative values separately */
    sprintf (destptr, "E%d", exponent);
  }
  else
  {
    *destptr = '\0';
  }
}


/* The procedure converts the contents of a string variable to a string. */
void sv2str (uchar *source, int maxlen, char *destination)
{
  int i, j, c, quot, plus;

  quot = 0;		/* 0/1 = even/odd number of quotes */
  plus = 0;		/* '+' required when not zero */
  j = (maxlen > 8) ? -1 : 1;

  for (i=0; i<maxlen; i++)
  {
    c = (int) *source++;
    if (i != j)		/* skip the string variable identifier */
    {

/* end of the string? */
      if (c == 0)
      {
        break;
      }

/* printable character? */
      else if (c < 0xC0 && characters[c] != '~')
      {
        if (plus != 0)
        {
          *destination++ = '+';
          plus = 0;
        }
        if (quot == 0)
        {
          *destination++ = '\042';
          quot = 1;
        }
        *destination++ = characters[c];
      }

/* unprintable character */
      else
      {
        if (quot != 0)
        {
          *destination++ = '\042';
          quot = 0;
          plus = 1;
        }
        if (plus != 0)
        {
          *destination++ = '+';
        }
        sprintf (destination, "CHR(%d)", c);
        while (*++destination != '\0')
          ;
        plus = 1;
      }
    }
  }

  if (quot != 0)
  {
    *destination++ = '\042';
  }

/* terminate the output string with 0 */
  *destination = '\0';
}


int main(int argc, char *argv[])
{
  int i, x;
  uint vars, ramtop;
  FILE *fp;

  if (argc<=1)
  {
    fprintf (stderr, "\nMissing file name, program aborted\n");
    return 1;
  }

  if ((fp = fopen(*++argv, "rb")) == NULL)
  {
    fprintf (stderr, "\nCannot open the file %s\n", *argv);
    return 1;
  }

/* special string variable $ */
  (void) fseek (fp, (long) 0x014E, SEEK_SET);
  for (i=0; i<30; i++)
  {
    if ((x = getc(fp)) == EOF)
    {
      (void) fclose (fp);
      fprintf (stderr, "\nPremature end of the file\n");
      return 1;
    }
    inbuf[i] = (uchar) x;
  }
  if (inbuf[0] != '\0')
  {
    sv2str (inbuf, 30, outbuf);
    printf ("$ = %s\n", outbuf);
  }

/* read the 'vars' and 'ramtop' system variables */
  (void) fseek (fp, (long) 0x0250, SEEK_SET);
  for (i=0; i<4; i++)
  {
    if ((x = getc(fp)) == EOF)
    {
      (void) fclose (fp);
      fprintf (stderr, "\nPremature end of the file\n");
      return 1;
    }
    inbuf[i] = (uchar) x;
  }
  vars = get_word (inbuf);
  ramtop = get_word (inbuf+2);
  if (0x826B + 8*vars > ramtop)
  {
    (void) fclose (fp);
    fprintf (stderr, "\nInvalid data\n");
    return 1;
  }

/* print the remaining variables */
  (void) fseek (fp, (long) (ramtop - 8*vars - RAMSTART), SEEK_SET);
  while (vars-- != 0)
  {

/* read the variable from the file */
    for (i=0; i<8; i++)
    {
      if ((x = getc(fp)) == EOF)
      {
        (void) fclose (fp);
        fprintf (stderr, "\nPremature end of the file\n");
        return 1;
      }
      inbuf[i] = (uchar) x;
    }

/* skip empty variables */
    if (get_word (inbuf) == 0)
    {
      continue;
    }

/* print the name of the variable */
    if (vars >= 26)
    {
      printf ("Z(%u) = ", vars-25);
    }
    else
    {
      printf ("%c = ", characters[65+vars]);
    }

/* print the contents of the variable */
    if ((inbuf[1] & 0x60) == 0)
    {
      fp2str (inbuf, -10, outbuf);
    }
    else
    {
      sv2str (inbuf, 8, outbuf);
    }
    printf ("%s\n", outbuf);
  }

  (void) fclose (fp);
  return 0;
}
