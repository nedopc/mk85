# nedoMK85
Main purpose of this project is to create a hardware clone of
Soviet pocket computer "Elektronika MK 85" (that had face of Casio FX-700P,
but internals compatible with PDP-11) using KM1801VM2 CPU and discrete logic
components with extended memory (32KB) for http://nedoPC.org

I started from updating an emulator from
http://www.pisi.com.pl/piotr433/mk85emue.htm
with help of its maintainer - Piotr Piatek.

List of contributors to original MK85 emulator written in Delphi Pascal:
* Piotr Piatek - main author and maintainer of MK85 emulator and also author of all ROM extensions
* Aleksei Akatov - initial author of unfinished version of MK85 emulator
* V.A.Ovsienko - author of PDP-11/03 emulator that was rewritten to Pascal by Piotr
* Martin Minow - author of PDP-11 disassembler that was rewritten to Pascal by Piotr

Source code of emulator was modified for Lazarus to work in both Windows and Linux (thanks to Piotr Piatek).

Display was extended to include 5th digit for free memory size (to support 32K) and indication for TURBO and INS modes:

![](https://gitlab.com/nedopc/mk85/raw/master/mk85scr2018.gif "display")

Background image also was changed to avoid any confusion with original MK85 emulator ;)

![](https://gitlab.com/nedopc/mk85/raw/master/nedoMK-85MM.gif "screenshot")

Usage hints from original emulator:

* The files rom.bin and ram.bin contain the ROM and RAM images respectively. They are loaded when the program is started.
The updated ram.bin file is saved when the program is terminated. If the ram.bin file wasn't found, it will be created. 
In such case the memory has to be initialised with the key F8 or the TEST command.
* The emulator can be operated with the mouse or the keyboard. Special function keys:

```
    Insert:  MODE
    Left:    <-
    Right:   ->
    Home:    [S]
    End:     [F]
    Esc:     AC
    Delete:  DEL
    Enter:   EXE
    F2:      switches key labels between normal and graphic characters
    F3:      suspends the code execution and opens the debugger window
    F8:      RAM initialisation (equivalent to the key on the back side)
```

I took ROM extension from http://mk85.republika.pl/rom.html
to use as a base for firmware of my clone (Piotr Piatek is author of the patch).
Sources of the patch were modified to be compatible with PDP11ASM
( fixed version available here: https://gitlab.com/shaos/pdp11asm85 )
and extended.

Finally a picture of whole model line of MK-85 ;)

![](https://gitlab.com/nedopc/mk85/raw/master/mk85.jpg "family")

