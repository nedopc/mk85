; This patch to original MK85 ROM was based on PATCH27.PSM from Piotr Piatek
; Translated to English, adapted to PDP11ASM and extended by Shaos <me@shaos.net>
; Git repository for nedoMK85 project: https://gitlab.com/nedopc/mk85

; Current version: 28 beta

; 2018-01-21 - conversion from PSM to ASM compatible with modified PDP11ASM
; 2018-01-26 - Piotr added function ITOA and removed ORG directives after 4200H
; 2018-01-31 - Piotr fixed function ITOA (v28beta)

decimalnumbers

temp1=8244H
temp2=8246H

; *** changes in original ROM ***

	org     0
insert_file	"mk85.rom"

; handling keys EE and H (for hexadecimal entry)
	org	0216H
	jmp	hexinp

; typing chatacter R0 into edit buffer with support of INSERT mode
	org	0270H
	jmp	ins1

; new vectrors MODE7 and MODE8
	org	02E6H
	.word	mode7
	.word	mode8

; vector to handling keys DEL/INS
	org	046AH
	.word	del1

; modified procedure of cheching of memory amount in RAM
	org	0662H
	jmp	ramchk

; change memory configuration and switching of PP4-PP11 ports on the input
; #0C26H for RAM above 16kB - memory configuration 1: 00001 (4 x 8kB)
; #0C86H for RAM below 16kB - memory configuration 3: 00100 (8 x 2kB)
	org	0B44H
	mov	#0C26H,@#0104H

; adding autostart logic to program that ends with RUN
	org	0B98H
	jmp	auto1

; adding new commands and functions 
	org	0BFCH
	jmp	func3

; changing test procedure for ROM checksum to support 32kB
	org	102EH
	mov	#3F7CH,r0

; adding new commands
	org	16C2H
	jmp	cmd1

; vector to handle new logic for MODE
	org	1704H
	.word	mode

; table of functions requiring multiple arguments enclosed within parentheses
	org	1D88H
	mov	#l1d9c,r0

	org	1E24H
	mov	#l1d9c,r1

; extension of function argument parser to support new functions
	org	1E50H
	.word	func1

; extension to handle hexadecimal numbers preceded by &H
	org	1F3EH
	jmp	hexlit

; adding new functions
	org	2246H
	jmp	func6

; changing address of the table of keywords for parser
	org	26C0H
	mov	#tokens,r2

; changing address of the table of keywords for list
	org	2746H
	mov	#tokens,r2

; no checksum here for 32kB ROM
	org	3FFEH
	.word	0

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; *** extensions for 2nd half of the ROM ***

; new table of BASIC keywords instead of 27BCh
	org	04000H
tokens:	.byte	"SIN ",0	;code C0
	.byte	"COS ",0	;code C1
	.byte	"TAN ",0	;code C2
	.byte	"ASN ",0	;code C3
	.byte	"ACS ",0	;code C4
	.byte	"ATN ",0	;code C5
	.byte	"LOG ",0	;code C6
	.byte	"LN ",0		;code C7
	.byte	"EXP ",0	;code C8
	.byte	"SQR ",0	;code C9
	.byte	"ABS ",0	;code CA
	.byte	"INT ",0	;code CB
	.byte	"SGN ",0	;code CC
	.byte	"FRAC ",0	;code CD
	.byte	"VAL ",0	;code CE
	.byte	"LEN ",0	;code CF
	.byte	"CHR ",0	;code D0
	.byte	"ASCI ",0	;code D1
	.byte	"RND ",0	;code D2
	.byte	"MID ",0	;code D3
	.byte	"GETC ",0	;code D4
	.byte	"RAN#",0	;code D5
	.byte	"KEY",0		;code D6
	.byte	"CSR ",0	;code D7
	.byte	"NEXT ",0	;code D8
	.byte	"GOTO ",0	;code D9
	.byte	"GOSUB ",0	;code DA
	.byte	"RETURN",0	;code DB
	.byte	"IF ",0		;code DC
	.byte	"FOR ",0	;code DD
	.byte	"PRINT ",0	;code DE
	.byte	"INPUT ",0	;code DF
	.byte	" THEN ",0	;code E0
	.byte	" TO ",0	;code E1
	.byte	" STEP ",0	;code E2
	.byte	"STOP",0	;code E3
	.byte	"END",0		;code E4
	.byte	"LETC ",0	;code E5
	.byte	"DEFM ",0	;code E6
	.byte	"VAC",0		;code E7
	.byte	"MODE ",0	;code E8
	.byte	"SET ",0	;code E9
	.byte	"DRAWC ",0	;code EA
	.byte	"DRAW ",0	;code EB
	.byte	"RUN ",0	;code EC
	.byte	"LIST ",0	;code ED
	.byte	"AUTO ",0	;code EE
	.byte	"CLEAR ",0	;code EF
	.byte	"TEST",0	;code F0
	.byte	"WHO",0		;code F1
; new commands:
	.byte	"SAVE",0	;code F2
	.byte	"LOAD",0	;code F3
	.byte	"CALL ",0	;code F4
	.byte	"POKE ",0	;code F5
	.byte	"SDATA ",0	;code F6
	.byte	"I2CSTART",0	;code F7
	.byte	"I2CSTOP",0	;code F8
	.byte	"I2CWBYTE ",0	;code F9
	.byte	"OFF",0		;code FA
	.byte	"BEEP",0	;code FB
; new functions:  
	.byte	"PEEK ",0	;code FC
	.byte	"LDATA ",0	;code FD
	.byte	"I2CRBYTE ",0	;code FE
	.byte	"ITOA ",0	;code FF
emptys:	.byte	0

; functions requiring multiple arguments enclosed within parentheses
l1d9c:	.byte	0D2H, 0D3H, 0D4H, 0FFH, 00	;RND, MID, GETC, ITOA

msg1:	.byte	"Loading...",0

; (not supported by pdp11asm)	.even

	org	4200H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; execution of new commands and functions, previously located at 0BFCh
func3:	cmpb	r0,#0FCH	;code of the first new function @@@
	bcs	l0BFC
func4:	jmp	@#0C0AH
l0BFC:	sub	#0FFE5H,r0	;E5-FB - direct mode command codes @@@
	bcs	func4
	asl	r0
	inc	r2
	jmp	@cmdtab(r0)

; extended table of jump addresses, previously located at 0DD2h
cmdtab:	.word	144EH		;LETC
	.word	14C4H		;DEFM
	.word	1598H		;VAC
	.word	mode		;MODE
	.word	1630H		;SET
	.word	1CB8H		;DRAWC
	.word	1CBEH		;DRAW
	.word	0DECH		;RUN
	.word	0E7AH		;LIST
	.word	0F52H		;AUTO
	.word	0FACH		;CLEAR
	.word	1006H		;TEST
	.word	1170H		;WHO
cmdt1:	.word	save		;SAVE
	.word	load		;LOAD
	.word	calll		;CALL
	.word	poke		;POKE
	.word	sdata		;SDATA
	.word	istart		;I2CSTART
	.word	istop		;I2CSTOP
	.word	iwbyte		;I2CWBYTE
	.word	pwroff		;OFF
	.word	beep		;BEEP

funtab:	.word	peek		;PEEK
	.word	ldata		;LDATA
	.word	irbyte		;I2CRBYTE
	.word	itoa		;ITOA

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; extension of the argument parser at 1E14h to support new functions
func1:	cmpb	(r2),#0FCH	;code of the first new function @@@
	bcs	func2
	jmp	@#1E22H
func2:	jmp	@#1E14H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; adding new commands from the program level (previously at 16C2h)
cmd1:	cmp	#0014H,r0
	bcs	cmd2
	jmp	@#16CCH		;original commands from D7 to EB @@@
cmd2:	sub	#001BH,r0
	bcc	cmd3
err2b:	jmp	@#1CACH		;ERR2
cmd3:	cmp	#0009H,r0	;@@@
	bcs	err2b
; execution of new commands with codes from F2 to FB @@@
	asl	r0
	inc	r2
	jmp	@cmdt1(r0)

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; adding new functions to the code from 2246h
func6:	cmpb	r1,#0FCH	;code of the first new function @@@
	bcc	func7
	cmpb	r1,#0C0H
	jmp	@#224Ah
func7:	cmpb	#0FFH,r1	;@@@
	bcs	err2b
	bic	#0FF00H,r1
	asl	r1
	jmp	@funtab-01F8H(r1)	;2*FCh=1F8h @@@

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; interpretation of function PEEK
peek:	jsr	pc,@#266CH	;expect number on the stack
	jsr	r4,@#3AA8H
	.word	fl2int		;conversion float -> integer
	.word	peek1
peek1:	bcc	peek2		;jump if conversion was successful
err5:	jmp	@#25FCH		;ERR5
; argument is correct, now test address range (MUST BE CHANGED !!!)
peek2:	cmp	(sp),#0100H
	bcs	peek3
	cmp	(sp),#0106H
	bcc	peek3
; reading word from memory
	mov	@0(sp),(sp)
	jsr	r4,@#3AA8H
	.word	int2fl		;conversion integer -> float
	.word	226CH
; reading byte from memory
peek3:	movb	@0(sp),(sp)
	bic	#0FF00H,(sp)
	jmp	@#259AH

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; interpretation of function LDATA
ldata:	jsr	pc,@#266CH	;expect number on the stack
	jsr	r4,@#3AA8H
	.word	fl2int		;conversion float -> integer
	.word	ldata1
ldata1:	bcs	err5		;ERR5 if conversion was unsuccessful
; argument is correct, now row search (copied from GOTO)
	mov	(sp),r0
	cmp	#9999,r0        ;9999 decimal!!!
	bcs	err5		;ERR5 if number is above 9999
	mov	#823EH,r5	;adress of the end of the program P9
	mov	@#8248H,-(sp)
	jsr	pc,@#1196H	;r1 <- row address r0
	bne	ldata2		;jump if row was not found
; row was found
	cmpb	(r1)+,(r1)+	;omit the line number
	cmpb	#21H,(r1)+	;test for exclamation mark
	beq	ldata3
; row was not found, an empty string will be returned
ldata2:	mov	#emptys,r1
ldata3:	mov	r1,r0
	mov	(sp)+,@#8248H
; calculating the length of the line
ldata4:	tstb	(r0)+
	bne	ldata4
	sub	r1,r0
	dec	r0		;length without terminal zero
	mov	r0,-(sp)	;the length of the string
	mov	r1,-(sp)        ;adress of the string
	mov	#4080H,-(sp)	;identifier
	jmp	@#226CH

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; changed procedure to test RAM to move final address to FFFEh
; otherwise the computed end of RAM 32kB will be 10000h
ramchk:	mov	#8000H,r0
ramch1:	add	#0800H,r0 ; step is 2kB
	bcs	ramch2 ; branch if carry set (end of loop)
	mov	#0A27EH,(r0)
	cmp	#0A27EH,(r0)
	beq	ramch1
	rts	pc
; the end of the address space reached
ramch2:	mov	#0FFFEH,r0
	rts	pc

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; handling [MODE] 7
mode7:	bic	#8,@#0104H	;normal frequency
	jmp	@#0108H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; handling [MODE] 8
mode8:	bis	#8,@#0104H	;turbo mode
	jmp	@#0108H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; extension of the MODE command with arguments 7 and 8
mode:	cmpb	(r2),#37H
	beq	slow
	cmpb	(r2),#38H
	beq	fast
	jmp	@#15A0H
slow:	bic	#8,@#0104H	;normal frequency
	br	clock2
fast:	bis	#8,@#0104H	;turbo mode
clock2:	inc	r2
	jmp	@#1652H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; entering hexadecimal numbers using the EE and H keys
hexinp:	cmpb	#48H,r0	;'H'
	beq	hexin1
	cmpb	#2DH,r0	;'-'
	jmp	@#021AH
hexin1:	mov	@#8260H,r3	;pointer to the input buffer
	cmpb	#7BH,-(r3)	;'E', exponent
	beq	hexin2
	jmp	@#0226H
hexin2:	mov	#26H,r0	;'&'
	movb	r0,(r3)
	decb	@#8269H		;cursor position
	jsr	pc,@#09F8H	;displaying the sign of r0
	incb	@#8269H
	mov	#48H,r0	;'H'
	jmp	@#0226H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; auto-start of the first program that has RUN statement at the end
auto1:	cmp	#0110H,@#0100H	;reading keyboard columns
	beq	auto6		;skip the autostart when dot or space pressed
	mov	#822CH,r5	;array of addresses of ends of BASIC programs 0-9
	mov	#826BH,r4	;the beginning of BASIC programs
	clr	r0		;initial program number (0)
auto3:	cmp	r4,(r5)		;comparison of the beginning and end of the program
	mov	(r5)+,r4
	bcc	auto5		;omit empty programs
	cmpb	#0ECH,-2(r4)	;is the RUN statement at the end of the program?
	bne	auto5
; program execution number R0, cleaning the display copied from MODE 0
	movb	r0,@#8268H	;selected program
	bic	#0C7B1H,@#8256H
	bicb	#2,@#8008H
	bisb	#1,@#8008H
	movb	@#8008H,@#88H
	mov	#8030H,r1
	mov	#6,r0
auto4:	clrb	(r1)
	clrb	8080H(r1)
	add	#8,r1
	sob	r0,auto4
	jmp	@#0E08H		;RUN from line 1
; nastepny program
auto5:	inc	r0		;next program number
	cmp	r5,#8240H	;is it end of programs table?
	bcs	auto3
; no program for automatic execution is found
auto6:	jmp	@#02ECH		;mode 0

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; code parser at 1F3Eh extended for hexadecimal numbers preceded by &H
hexlit:	cmpb	#26H,(r2)	;'&'
	beq	hexli1
	cmpb	(r2),#30H	;'0'
	jmp	@#1F42H
hexli1:	inc	r2
	cmpb	#48H,(r2)+	;'H'
	beq	hexli2
err2c:	jmp	@#263EH		;ERR2
hexli2:	clr	temp2		;index cleared
	jsr	pc,hexdig
	bcs	err2c
	mov	r2,@#8254H
	jsr	r4,@#3AA8H
	.word	pshdig
	.word	hexli3
; looping to download subsequent HEX digits
hexli3:	mov	@#8254H,r2
	jsr	pc,hexdig
	mov	r2,@#8254H
	bcs	hexli8
	bis	#0400H,r0	;counter added
	mov	r0,temp1
; multiplying the number on the stack by 16
hexli4:	jsr	r4,@#3AA8H
	.word	hexpre
	.word	37A8H		;DUP
	.word	3840H		;adding BCD
	.word	hexli5
hexli5:	add	#8,sp
	decb	temp1+1
	bne	hexli4
; adding a number to the number on the stack
	mov	temp1,r0
	jsr	r4,@#3AA8H
	.word	hexpre
	.word	pshdig
	.word	3840H		;adding BCD
	.word	hexli6
hexli6:	add	#8,sp
; limiting the result to 14 digits (to have room for the next digit)
hexli7:	bit	#0FF00H,(sp)
	beq	hexli3
	inc	temp2		;index incremented
	mov	sp,r2
	add	#8,r2
	jsr	r4,@#3AA8H
	.word	3986H		;moving the number one digit to the right
	.word	hexli7
; conversion of the result into a floating-point format
hexli8:	tst	(sp)
	beq	hexli9
	inc	temp2		;index incremented
	mov	sp,r2
	add	#8,r2
	jsr	r4,@#3AA8H
	.word	3986H		;moving the number one digit to the right
	.word	hexli8
; normalization of the result if it's different from zero
hexli9:	tst	temp2		;check index
	beq	hexlic          ;for zero
hexlia:	bit	#0F000H,2(sp)
	bne	hexlib
	dec	temp2		;index decremented
	mov	sp,r2
	add	#8,r2
	jsr	r4,@#3AA8H
	.word	3970H		;moving the number one digit to the left
	.word	hexlia
hexlib:	mov	temp2,(sp)
hexlic:	mov	@#8254H,r2
	mov	@#8248H,r5
	jmp	@#1F80H

; returns in R0 the value of the HEX digit pointed to by R2
; carry set when R2 does not indicate the HEX digit
hexdig:	movb	(r2),r0
	sub	#30H,r0
	bcs	hexdi2
	cmp	#9,r0
	bcc	hexdi1
	sub	#7,r0
	cmp	r0,#0AH
	bcs	hexdi2
	cmp	#0FH,r0
	bcs	hexdi2
	add	#6,r0	;conversion to BCD
hexdi1:	inc	r2
hexdi2:	rts	pc

; placing the BCD number in R0 on the 2-digit stack
pshdig:	tst	r0
	beq	pshdi4
	tst	temp2		;check index
	bne	pshdi1
; it was the first HEX digit different from zero
	mov	#100CH,temp2
	br	pshdi4
; move right depending on the index
pshdi1:	cmp	#100DH,temp2
	bcs	pshdi3
	bne	pshdi4
	asr	r0
	br	pshdi4
pshdi3:	clr	r0
pshdi4:	mov	r0,-(sp)
	clr	-(sp)
	clr	-(sp)
	clr	-(sp)
	jmp	@(r4)+

; preparing in R3 and R5 indicators for added numbers on the stack
hexpre:	mov	sp,r5
	mov	sp,r3
	add	#8,r3
	jmp	@(r4)+

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; modified support for the DEL/INS key
; DEL at the end of the line works like Backspace
; INS switches the overwrite/insert modes, memorized in bit 13 at 8256h
del1:	tstb	@#8264H		;flags
	bpl	del2
	bit	#20H,r2		;mode [S] ?
	beq	del4		;jump if not
; pressed INS
	mov	#2000H,r0
	xor	r0,@#8256H
del2:	jmp	@#050AH
; pressed DEL
del4:	movb	@#8269H,r0	;cursor position
	mov	@#8260H,r1	;pointer to the editing buffer
	tstb	(r1)		;end of the line?
	beq	del5		;jump if yes
	jmp	@#0526H
del5:	cmp	#816DH,r1	;the beginning of a line?
	bcc	del2		;jump if yes
	clrb	-(r1)		;deleting the last character in the edit buffer
	mov	r1,@#8260H	;pointer to the editing buffer
	decb	@#8269H		;cursor position
	mov	#20H,r0		;space
	jsr	pc,@#09F8H	;printing character R0
	cmp	@#8260H,#8178H	;less than 11 characters in the buffer?
	bcs	del2		;jump if yes
	jmp	@#048EH		;scroll to the left of the display

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; entering the R0 character into the edit buffer
ins1:	tstb	@8260H		;end of buffer?
	beq	overw1		;jump if yes
	bit	#2000H,@#8256H	;insert mode?
	bne	ins2		;jump if yes

; inserting the R0 character at the cursor position in overwrite mode, 
; continuation of the original code at 0270H
overw1:	cmp	@#8260H,#81ACH
	bcc	overw2
	jmp	@#0278H
overw2:	jmp	@#025CH

; inserting the R0 character at the cursor position in insert mode,
; modification of the original code at 057EH, inserting spaces
ins2:	mov	@#8260H,r1	;pointer to the editing buffer
ins3:	tstb	(r1)+
	bne	ins3
	cmp	r1,#81ADH	;end of editing buffer?
	bcc	ins8		;jump if there is no space
ins4:	movb	-(r1),1(r1)	;make space in the edit buffer
	cmp	@#8260H,r1
	bcs	ins4
	movb	r0,(r1)		;inserting new character into the buffer
	mov	r3,-(sp)
	movb	@#8269H,r3	;cursor position
	sub	#0BH,r3
	bcc	ins7
	neg	r3
	mov	#8058H,r1	;end of LCD memory
ins5:	mov	#7,r2		;row counter of the character
ins6:	movb	-(r1),8(r1)	;making space in the LCD buffer
	movb	(r1),8088H(r1)	;sending byte to the LCD controller
	sob	r2,ins6		;next row
	dec	r1
	sob	r3,ins5		;next character
ins7:	mov	(sp)+,r3
	jsr	pc,@#09F8H	;displaying a new character
; move the cursor to the right, adapt the code at 04C4H
	tstb	@#8264H
	bpl	ins8
	tstb	@8260H
	beq	ins8
	inc	@#8260H
	cmpb	@#8269H,#0BH	;cursor position
	bcs	right5
; scroll the display to the left
	mov	#8000H,r1	;pointer to the LCD buffer
	mov	#0BH,r2		;character counter
; copying the last character
right2:	inc	r1		;skip unused byte
	mov	#7,r0		;row counter of the character
right3:	movb	8(r1),(r1)
	movb	(r1)+,807FH(r1)
	sob	r0,right3	;next row
	sob	r2,right2	;next character
	movb	@8260H,r0
	bne	right4
	mov	#020H,r0	;space
right4:	jsr	pc,@#09F8H	;displaying character R0
	br	right6
right5:	incb	@#8269H		;cursor position
right6:	jmp	@#0258H

; emergency exit
ins8:	jmp	@#050AH

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; interpretation of command BEEP (speaker is on PP11)
beep:	mov	#0800H,r0	;bit mask for PP11
	mov	#0400H,r1	;ticks counter
	mov	#0FF0H,@#0102H	;SCL=0, PP11=0
	bic	#4,@#0104H	;PP11..PP8 = exit
	mov	#5,-(sp)
	bit	#8,@#0104H
	beq	beep1		;jump for slower clock speed
	mov	#35,(sp)
beep1:	xor	r0,@#0102H
	mov	(sp),r3
beep2:	sob	r3,beep2
	sob	r1,beep1
	tst	(sp)+
	bis	#4,@#0104H	;PP11..PP8 = exit
	clr	@#0102H
	jmp	@#1652H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; interpretation of command POKE (copied from DRAW)
poke:	tstb	@#8256H		;mode
	bmi	err2		;ERR2 if mode WRT
	jsr	pc,evint	;r0 <- value of expression
	cmpb	#2CH,(r2)+	;comma ','?
	beq	poke1           ;jump if yes
err2:	jmp	@#1CACH		;ERR2, syntax error if no comma
poke1:	mov	r0,-(sp)	;adress
	jsr	pc,evint	;r0 <- value of expression
	mov	(sp)+,r1	;adress
; address range test (MUST BE CHANGED !!!)
	cmp	r1,#0100H
	bcs	poke2
	cmp	r1,#0106H
	bcc	poke2
; saving words to memory
	mov	r0,(r1)
	jmp	@#1652H
; saving byte to memory
poke2:	bit	#0FF00H,r0
	beq	poke3
	jmp	@#12C0H		;ERR5, argument out of range
poke3:	movb	r0,(r1)
	jmp	@#1652H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; interpretation of command CALL
calll:	tstb	@#8256H		;mode
	bmi	err2		;ERR2 if mode WRT
	jsr	pc,evint	;r0 <- value of expression
	mov	r2,-(sp)
	jsr	pc,(r0)
	mov	(sp)+,r2
	jmp	@#1652H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; interpretation of command SDATA
sdata:	tstb	@#8256H		;mode
	bmi	err2		;ERR2 if mode WRT
; first argument is line number
	jsr	pc,evint	;r0 <- value of expression
	cmpb	#2CH,(r2)+	;comma ','?
	bne	err2		;ERR2, syntax error if no comma
	cmp	#9999,r0        ;9999 decimal!
	bcc	sdata1
err5a:	jmp	@#25FCH		;ERR5 if line number greater than 9999
; second argument is a string (pulled from LETC)
sdata1:	mov	r0,-(sp)	;store the line number
	mov	#1000H,-(sp)
	jsr	pc,@#1D50H	;calculation of the expression
	bit	#4000H,(sp)	;expected string
	beq	err5a		;ERR5 if no string
; search in program P9 with the given number
	mov	#823EH,r5	;adress of the end of program P9
	mov	8(sp),r0	;line number
	clr	r3		;length of non-existing line is 0
	jsr	pc,@#1196H	;r1 <- line address r0
	bne	sdata3		;line not found
; line already exists, calculating its length in r3
	mov	r1,r3
	cmpb	(r3)+,(r3)+	;omit the line number
sdata2:	tstb	(r3)+
	bne	sdata2
	sub	r1,r3
; changed can only be made behind the point where program is executed
sdata3: cmp	r2,r1
	bcc	err5a
; matching the amount of space for a new line
	mov	#823EH,r5	;address of the end of BASIC programs
	mov	4(sp),r4	;length of the line
	beq	sdataa
	add	#4,r4		;length of the new line
sdataa:	mov	r1,-(sp)
	add	r3,r1		;address of the end of the previous line
	sub	r3,r4		;how many bytes you have to move
	beq	sdata8		;both lines have the same length
	bcs	sdata6		;new line is shorter
; the new row is longer
; first we need to calculate the amount of free memory
	mov	@#8250H,r0	;number of variables
	asl	r0
	asl	r0
	asl	r0		;r0 = number of bytes occupied by variables
	neg	r0
	add	@#8252H,r0	;adress of the end of RAM memory
	sub	(r5),r0		;adress of the end of BASIC programs
	cmp	r0,r4
	bcc	sdata4
	jmp	@#12AEH		;ERR1, not enough memory
; siding the memory - moving the block (from the end)
sdata4:	mov	(r5),r3
	add	r4,(r5)		;updating the address of the end of BASIC programs
	mov	r3,r0		;source address
	add	r3,r4		;destination address
	sub	r1,r3		;number of bytes to be moved
	beq	sdata8
sdata5:	movb	-(r0),-(r4)
	sob	r3,sdata5
	br	sdata8
; new line is shorter
; resolving the memory - moving the block (from the beginning)
sdata6:	mov	(r5),r3
	add	r4,(r5)		;updating the address of the end of BASIC programs
	mov	r1,r0		;source address
	add	r1,r4		;destination address
	sub	r1,r3		;number of bytes to be moved
	beq	sdata8
sdata7:	movb	(r0)+,(r4)+
	sob	r3,sdata7
; inserting new line
sdata8:	mov	(sp)+,r1	;destination address
	mov	(sp)+,r5	;type
	mov	(sp)+,r0	;source address
	mov	(sp)+,r3	;length
	tst	(sp)+		;unused
	mov	(sp)+,r4	;line number
; line deleted if length is 0
	tst	r3
	beq	sdatab
; entering line number
	movb	r4,(r1)+
	swab	r4
	movb	r4,(r1)+
; exclamation mark
	movb	#21H,(r1)+
; copying the string
	tstb	r5
	bne	sdata9		;jump if no character variable
; character value
	sub	#8,r0		;moving the pointer to the beginning of a variable
	movb	(r0)+,(r1)+	;first character
	inc	r0		;skip identifier 60h
	br	sdatac
sdata9:	movb	(r0)+,(r1)+
sdatac:	sob	r3,sdata9
; the end of line
	clrb	(r1)+
sdatab:	jmp	@#1652H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; interpretation of command SAVE
save:	jsr	pc,eedev	;reading device identifier (0..7)
	mov	#822CH,r4	;initial address of RAM
	clr	r5		;start address of EEPROM (0)
	jsr	pc,i2cpre
	jsr	pc,i2cres
	beq	err9
save1:	jsr	pc,eeaddr
save2:	mov	(r4)+,r0
	cmp	r4,#8240H
	bne	save3
	mov	#826AH,r4	;skip the area of system variables
; writing word
save3:	jsr	pc,i2c_wr
	jsr	pc,nack
	swab	r0
	jsr	pc,i2c_wr
	jsr	pc,nack
	add	r1,r5		;increasing the EEPROM address
	bit	#003FH,r5	;page bound test
	bne	save2
; the boundary of the EEPROM memory page
	jsr	pc,i2csto
; test, if everything is already saved
	tst	r5
	bmi	save4		;memory capacity of EEPROM exceeded
	tst	r4
	bpl	save4		;"scroll over" the address of the RAM
	cmp	r4,@#823EH
	bcs	save1
; end of writing
save4:	clr	r5
	jsr	pc,eeaddr
	jsr	pc,i2csto
	jsr	pc,i2cend
	jmp	@#1652H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; interpretation of command LOAD
load:	jsr	pc,eedev        ;reading device identifier (0..7)
	mov	#0041H,@#8264H	;flags (taken from WHO)
	mov	#msg1,r4	;text "Loading..."
	jsr	pc,@#1248H	;display text
	mov	#822CH,r4	;initial address of RAM
	clr	r5		;start adress of EEPROM (0)
	jsr	pc,i2cpre
	jsr	pc,i2cres
	beq	err9
	jsr	pc,eeaddr
	jsr	pc,i2csta
	movb	temp1,r0	;device select
	incb	r0		;read
	jsr	pc,i2c_wr
	jsr	pc,nack
; word reading
load1:	mov	#0101H,r0
	jsr	pc,i2c_rd
	jsr	pc,ack
	swab	r0
	jsr	pc,i2c_rd
	jsr	pc,ack
	swab	r0
	com	r0
	mov	r0,(r4)+
	cmp	r4,#8240H
	bcs	load1		;continuation of reading the address table
	bne	load2
	mov	#826AH,r4	;skip the area of system variables
; test, if everything is already loaded
load2:	tst	r4
	bpl	load3		;"scroll over" the address of the RAM
	cmp	r4,@#823EH
	bcs	load1
; end of reading
load3:	mov	#1,r0
	jsr	pc,i2c_rd	;read byte ignored
	jsr	pc,nack		;interruption of transmission
	jsr	pc,i2csto
	jsr	pc,i2cend
	jmp	@#0B40H		;restart of the system

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; sending the address r5 to the EEPROM memory, ERR9 error when the memory is not present
eeaddr:	mov	r5,-(sp)
	mov	#0100H,r5	;counter trying to get ACK confirmation
eeadd1:	jsr	pc,i2csta
	movb	temp1,r0	;device select + write
	jsr	pc,i2c_wr
	bis	r1,(r3)		;SDA=input
	bis	r2,(r3)		;SCL=input
	bit	#0080H,@#0102H	;SDA reading
	bne	eeadd2		;we leave the crib when SDA = 0, i.e. it's ACK
; no ACK, busy or absent memory
	jsr	pc,i2csto
	sob	r5,eeadd1	;try again
; prob limit exceeded, ERR 9 reported
err9:	jsr	pc,i2cend
	mov	#39H,r0
	jmp	@#12CAH
; if ACK, sending the remaining two bytes of the address
eeadd2:	bic	r2,(r3)		;SCL=output
	mov	(sp)+,r5
	mov	r5,r0
	swab	r0		;more significant byte of the address
	jsr	pc,i2c_wr
	jsr	pc,nack
	swab	r0		;less significant byte of the address
	jsr	pc,i2c_wr
	jsr	pc,nack
	rts	pc

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; get the optional memory number (in the range 0 to 7) and 
; calculate the device select byte in the variable temp1
eedev:	clr	r0		;default memory number (0)
	cmpb	(r2),#30H	;'0'
	bcs	eedev1
	cmpb	(r2),#38H	;'8'
	bcc	eedev1
	movb	(r2)+,r0
	bicb	#0F8H,r0
	aslb	r0
eedev1:	bis	#0A0H,r0
	movb	r0,temp1
	rts	pc

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; precedure of handling command I2CSTART
istart:	jsr	pc,i2cpre
	bit	r1,(r3)		;is the SDA line set at the input?
	beq	istar1		;bus reset if not
	bit	r2,(r3)		;does the SCL line set at the input?
	bne	istar2		;we skip the bus reset when it does
istar1:	jsr	pc,i2cres
	beq	err9
istar2:	jsr	pc,i2csta
	mov	temp2,r2
	jmp	@#1652H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; procedure of handling command I2CSTOP
istop:	jsr	pc,i2cpre
	jsr	pc,i2csto
	jsr	pc,i2cend
	jmp	@#1652H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; procedure of handling command I2CWBYTE
iwbyte:	jsr	pc,evint	;r0 <- value of expression
	bit	#0FF00H,r0
	beq	iwbyt2
	jmp	@#12C0H		;ERR5, argument out of range
iwbyt2:	jsr	pc,i2cpre
	jsr	pc,i2c_wr
	jsr	pc,nack
	mov	temp2,r2
	jmp	@#1652H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; procedure of handling command I2CRBYTE
irbyte:	jsr	pc,@#266CH	;expected number on the stack
	jsr	pc,i2cpr1
	mov	#1,r0
	jsr	pc,i2c_rd
	comb	r0
	tst	(sp)		
	bne	irbyt2
irbyt1:	jsr	pc,ack
	br	irbyt3
irbyt2:	jsr	pc,nack
irbyt3:	add	#6,sp
	mov	r0,(sp)
	jmp	@#259AH

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; interpretation of command OFF
pwroff:	jsr	pc,i2cpre
	jsr	pc,i2cend
pwrof1:	bis	#1000H,(r3)
	br	pwrof1		;actually unnecessary, required by the emulator

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; *** procedures of handling EEPROM ***
; PP7 - SDA, the bit number chosen deliberately
; PP8 - SCL, it could be any, but in a different group than SDA

; constant initiation used by I2C bus operation procedures
i2cpre:	mov	r2,temp2
i2cpr1:	mov	#2,r1		;bit mask for SDA
	mov	#4,r2		;bit mask for SCL
	mov	#0104H,r3	;adress of configuration register
	rts	pc

; initialization and reset of the I2C bus
; returns the set Z flag when failure
i2cres:
; initialization of the ports
; pre-low level on SDA and SCL, thus it will also be detected
; no pull-up resistors
	mov	#0FF0H,@#0102H	;SDA,SCL=0
	bic	r2,(r3)		;SCL=output
	bic	r1,(r3)		;SDA=output
	mov	#9,r0
	bis	r1,(r3)		;SDA=input
; we send up to 9 SCL pulses until we reach a high level on the SDA line,
; on the occasion of testing is also a high level on the SCL line
i2cre1:	bic	r2,(r3)		;SCL=output
	bis	r2,(r3)		;SCL=input
	bit	#0180H,@#0102H	;SDA and SCL reading
	beq	i2cre2		;we leave the cradle when SDA=1 and SCL=1
	sob	r0,i2cre1
; failure
	sez
	rts	pc		;Z=1
; start/stop pair
i2cre2:	bic	r1,(r3)		;SDA=output
; stop condition
i2csto:	bic	r2,(r3)		;SCL=output
	bic	r1,(r3)		;SDA=output
	bis	r2,(r3)		;SCL=input
	bis	r1,(r3)		;SDA=input, Z=0
	rts	pc

; start condition
i2csta:	bis	r1,(r3)		;SDA=input
	bis	r2,(r3)		;SCL=input
	bic	r1,(r3)		;SDA=output
	bic	r2,(r3)		;SCL=output
	rts	pc

; restoring the default status of the port 
i2cend:	clr	@#0102H
	bic	#6,(r3)		;SDA,SCL=outputs
	mov	temp2,r2
	rts	pc

; sending an R0 byte on the I2C bus
i2c_wr:	sec
	rolb	r0
i2c_w1:	bcc	i2c_w2
	bis	r1,(r3)		;SDA=input
	br	i2c_w3
i2c_w2:	bic	r1,(r3)		;SDA=output
i2c_w3:	bis	r2,(r3)		;SCL=input
	bic	r2,(r3)		;SCL=output
	aslb	r0
	bne	i2c_w1
	rts	pc

; readout of the negated byte from the I2C bus to the register R0
; before calling in the less significant byte R0 must be 01h
i2c_rd:	bis	r1,(r3)		;SDA=input
i2c_r1:	bis	r2,(r3)		;SCL=input
	mov	@#0102H,r5
	bic	r2,(r3)		;SCL=output
	rolb	r5		;SDA -> Carry
	rolb	r0
	bcc	i2c_r1
	rts	pc

; sending an ACK bit
ack:	bic	r1,(r3)		;SDA=output
	bis	r2,(r3)		;SCL=input
	bic	r2,(r3)		;SCL=output
	rts	pc

; sending a NACK bit or reading and ignoring the ACK bit
nack:	bis	r1,(r3)		;SDA=input
	bis	r2,(r3)		;SCL=input
	bic	r2,(r3)		;SCL=output
	rts	pc

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; converting a floating-point number to a 16-bit integer without a sign,
; returns the C flag set in case of conversion failure
; it's an adaptation of the system procedure at the address 2F72h
fl2int:	mov	sp,r3
	clr	r1
	mov	(r3)+,r2
	beq	fl2in3		;number 0
	bmi	fl2in4		;error if number is negative
	add	#0F000H,r2
	ble	fl2in3		;result 0 for numbers <1
	cmp	#5,r2		;test
	bcs	fl2in4		;number too large
; shifting the most significant mantissa digit to -(sp)
fl2in1:	mov	#4,r0
	clr	-(sp)
fl2in2:	asl	2(r3)
	rol	(r3)
	rol	(sp)
	sob	r0,fl2in2
	cmp	#6553,r1	;6553 decimal!!!
	bcs	fl2in4		;result out of range
; r1 = 10decimal * r1 + (sp)+
	asl	r1
	add	r1,(sp)
	asl	r1
	asl	r1
	add	(sp)+,r1
	bcs	fl2in4
	sob	r2,fl2in1
; OK, the result returned on the stack and C flag is cleared
fl2in3:	add	#6,sp
	mov	r1,(sp)
	jmp	@(r4)+
; error, returned 0 on the stack and C flag is set
fl2in4:	add	#6,sp
	clr	(sp)
	sec
	jmp	@(r4)+

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; calculation of the expression, expected result in the range of 0..65535
; it is an adaptation of the system procedure at 1220h
evint:	mov	#1000H,-(sp)
	jsr	pc,@#1D50H	;calculation of the expression
	bit	#6000H,(sp)
	bne	evint2
	mov	r2,@#8242H
	jsr	r4,@#3AA8H
	.word	fl2int
	.word	evint1
evint1:	mov	@#8242H,r2
	mov	(sp)+,r0	;score
	bcc	evint3		;jump when the conversion was successful
evint2:	jmp	@#12C0H		;ERR5
evint3:	rts	pc

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; converting a 16-bit unsigned integer on a stack to float,
; it's an adaptation of the system procedure at the address 2F18h
int2fl:	clr	r1		;mantissa
	clr	r3		;input number = 0
	mov	(sp),r2		;entry number
	bne	int2f1
	jmp	@#2F68H		;jump when the input number = 0
int2f1:	clr	(sp)		;deleting a character, a positive number
	jmp	@#2F24H

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; interpretation of function ITOA (copied from MID)
itoa:	jsr	pc,2676H	;expected two FP numbers on the stack
	jsr	r4,3AA8H
	.word	fl2int		;conversion float -> integer
	.word	itoa1
itoa1:	bcc	itoa3		;skip if conversion was successful
itoa2:	jmp	@#25FCH		;otherwise ERR5
itoa3:	mov	(sp)+,r5	;radix
	jsr	r4,3AA8H
	.word	fl2int		;conversion float -> integer
	.word	itoa4
itoa4:	bcs	itoa2		;ERR5 if conversion to integer failed
	mov	(sp)+,r0	;value
	cmp	r5,#2
	bcs	itoa2		;ERR5 if radix<2
	cmp	#36,r5
	bcs	itoa2		;ERR5 if radix>36
; r3 = pointer to the destination buffer below the variables area,
; code copied from DEFM
	mov	@#8250H,r3	;number of variables
	asl	r3
	asl	r3
	asl	r3		;r3 = number of bytes occupied by the variables
	neg	r3
	add	@#8252H,r3	;top of the RAM
; conversion loop
	clr	r4		;counter of characters
itoa5:	cmp	@#823EH,r3	;end of BASIC programs
	bcc	itoa7		;ERR1 if out of memory
	mov	r0,r1
	clr	r0
	.word	7205H		;div r5,r0 (not supported by pdp11asm)
	add	#'0',r1		;remainder
	cmp	#'9',r1
	bcc	itoa6
	add	#7,r1
itoa6:	movb	r1,-(r3)
	inc	r4
	tst	r0		;quotient
	bne	itoa5
	clr	-(sp)		;not used
	mov	r4,-(sp)	;length of the string
	mov	r3,-(sp)	;address of the string
	mov	#4080H,-(sp)	;"string" mark
	jmp	@#226CH
; ERR1 handler (out of memory error)
itoa7:	mov	#'1',r0
	jmp	@#265AH

; <><><><><><><><><><><><><><><><><><><><><><><><><>

; ROM checksum at the end (at 7FFEh and 7FFFh) calculated by PDP11ASM:

make_mk85_rom "rom.bin",32768
