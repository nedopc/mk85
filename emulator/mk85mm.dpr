program Mk85mm;

uses
  Forms, Interfaces,
  Main in 'main.pas' {MainForm},
  Def in 'def.pas',
  Cpu in 'cpu.pas',
  Decoder in 'decoder.pas',
  Exec in 'exec.pas',
  Srcdst in 'srcdst.pas',
  Numbers in 'numbers.pas',
  Debug in 'debug.pas' {DebugForm},
  Pdp11dis in 'pdp11dis.pas',
  Keyboard in 'keyboard.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Elektronika MK-85MM Emulator';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TDebugForm, DebugForm);
  Application.Run;
end.
