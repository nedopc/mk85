{ user interface for the MK85 emulator based on unfinished project written
  by Aleksei Akatov (Arigato Software) }
unit Main;

{ macro for extended version for Lazarus }

{$DEFINE MK85MM}

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
{$IFDEF MK85MM}
  ExtCtrls, IniFiles, LCLType;
{$ELSE}
  ExtCtrls, StdCtrls, IniFiles, ThdTimer;
{$ENDIF}

type
  TMainForm = class(TForm)
    RefreshTimer: TTimer;
    CursorTimer: TTimer;
{$IFDEF MK85MM}
    RunTimer: TTimer;
{$ELSE}
    RunTimer: TThreadedTimer;
{$ENDIF}
    LcdImage: TImage;
    procedure OnRefreshTimer(Sender: TObject);
    procedure OnCursorTimer(Sender: TObject);
    procedure OnRunTimer(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormPaint(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure ApplicationDeactivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
    MainForm: TMainForm;

implementation

{$R *.dfm}

uses
    Def, Cpu, Keyboard, Debug;

const
    FaceName: string = 'face.bmp';
    KeysName: string = 'keys.bmp';
    OverlayName: string = 'overlay.bmp';
    RomName: string = 'rom.bin';
    RamName: string = 'ram.bin';
    LoadMsg: string = 'Failed to load the file ';
    SaveMsg: string = 'Failed to save the file ';

var
    BitMap, Face, LcdBmp, KeyBmp, OverlayBmp: TBitMap;
    RedrawReq: Boolean = False;	{ true if the LcdBmp image has changed and needs
				  to be redrawn }

{ mouse }
    MD: Boolean;		{ True when the left mouse button was pressed
				  while it didn't point at any clickable
				  object (key, power switch). Intended action:
				  dragging the face image to another place. }
    XPos, YPos: integer;	{ mouse coordinates when left button pressed }

{ LCD }
    CurVis: Boolean = False;
    Scr: array[0..95] of byte;	{ LCD shadow memory }

{ CPU }
    CpuSpeed: integer;		{ how many instructions executes the emulated
				  CPU at each RunTimer event call }


{ draws the image of a key from the KeyBmp }
procedure DrawKey (index, x, y: integer; pressed: boolean);
var
  offset: word;
begin
  with keypad[index] do
  begin
    BitMap.Width := W;
    BitMap.Height := H;
    if (pressed) then offset := 0 else offset := H;
    BitMap.Canvas.Draw (-OX, -OY - offset, KeyBmp);
  end {with};
  BitMap.TransparentColor := $0000FF00;
  BitMap.Transparent := True;
  Face.Canvas.Draw (x, y, BitMap);
  BitMap.Transparent := False;
  MainForm.Invalidate;
end {DrawKey};


{ display a status sign }
procedure ViewHlp (
	x1: integer;
	y1: integer;	{ destination coordinates of the upper-left corner }
	x2: integer;
	y2: integer;	{ source coordinates of the upper-left corner }
        w: integer;	{ image width, height is always 7 pixels }
	what: byte);	{ draw image if not zero, clear if zero }
begin
  if what <> 0 then
  begin		{ copy a selection from the KeyBmp }
    with BitMap do
    begin
      Transparent := False;
      Width := w;
      Height := 7;
      Canvas.Draw (-x2, -y2, KeyBmp);
    end {with};
    LcdBmp.Canvas.Draw (x1, y1, BitMap);
  end
  else
  begin		{ clear the area }
    with LcdBmp.Canvas do
    begin
      Brush.Style := bsSolid;
{$IFDEF MK85MM}
      Brush.Color := clBlack;
{$ELSE}
      Brush.Color := clWhite;
{$ENDIF}
      FillRect (Rect(x1, y1, x1+w, y1+7));
    end {with};
  end {if};
  RedrawReq := True;
end {ViewHlp};


{ display a segment of the 7-segm. area }
procedure View7seg (
	x1: integer;
	y1: integer;	{ coordinates of the begin of the line }
	x2: integer;
	y2: integer;	{ coordinates of the end of the line }
	what: byte);	{ show segment if not zero, clear if zero }
begin
  with LcdBmp.Canvas do
  begin
    Brush.Style := bsSolid;
{$IFDEF MK85MM}
    if what <> 0 then Brush.Color := clRed
                 else Brush.Color := clBlack;
{$ELSE}
    if what <> 0 then Brush.Color := clBlack
                 else Brush.Color := clWhite;
{$ENDIF}
    FillRect (Rect(x1, y1, x2, y2));
  end {with};
  RedrawReq := True;
end {View7seg};


{ draw the LCD contents }
procedure View;
var
  Pos, Row, Col, Index, ZX, ZY, o1, o2, o3, o4: Integer;
  B: byte;
begin

{ draw the matrix }
  Index := 0;
  ZX := 2;	{ pixel coordinates }
  ZY := 24;

  with LcdBmp.Canvas do
  begin
    Brush.Style := bsSolid;
    for Pos := 0 to 11 do
    begin
      Inc (Index);
      for Row := 1 to 7 do
      begin
        B := lcd[Index];

{ handle the cursor }

        if CurVis and (Pos = (lcd[96] and $0F)) then
          if (Row = 7) or ((lcd[96] and $10) = 0) then B := $1F else B := 0;

        if Scr[Index] <> B then
        begin
          RedrawReq := True;
          Scr[Index] := B;
          for Col := 0 to 4 do
          begin
{$IFDEF MK85MM}
            if (B and 1) <> 0 then Brush.Color := clRed
                              else Brush.Color := clBlack;
{$ELSE}
            if (B and 1) <> 0 then Brush.Color := clBlack
                              else Brush.Color := clWhite;
{$ENDIF}
            B := B shr 1;
            FillRect (Rect(ZX, ZY, ZX+2, ZY+3));
            Inc (ZX, 3);
          end {for Col};
          Dec (ZX, 15);
        end {if};
        Inc (Index);
        Inc (ZY, 4);
      end {for Row};
      Inc (ZX, 19);
      Dec (ZY, 4*7)
    end {for Pos};
  end {with};

{$IFDEF MK85MM}
  o1 := 18;
  o2 := 14;
  o3 := 10;
  o4 := 6;
{$ELSE}
  o1 := 0;
  o2 := 0;
  o3 := 0;
  o4 := 0;
{$ENDIF}

{ draw the 7-segm. characters and the status signs }
  if Scr[$00] <> lcd[$00] then
  begin
    Scr[$00] := lcd[$00];
    ViewHlp (0, 5, 0, 56, 16, lcd[$00] and $01);	{ EXT }
    ViewHlp (19, 0, 17, 56, 5, lcd[$00] and $02);	{ S }
    ViewHlp (19, 9, 41, 56, 5, lcd[$00] and $04);	{ F }
  end {if};

  if Scr[$08] <> lcd[$08] then
  begin
    Scr[$08] := lcd[$08];
    ViewHlp (27, 0, 23, 56, 17, lcd[$08] and $01);	{ RUN }
    ViewHlp (26, 9, 47, 56, 19, lcd[$08] and $02);	{ WRT }
    ViewHlp (49, 5, 67, 56, 16, lcd[$08] and $10);	{ DEG }
  end {if};

{$IFDEF MK85MM}
  if Scr[$10] <> lcd[$10] then
  begin
    Scr[$10] := lcd[$10];
    View7seg (148, 3, 150, 8, lcd[$10] and $01);   { 0f }
    View7seg (149, 8, 154, 10, lcd[$10] and $02);  { 0g }
    View7seg (147, 9, 149, 14, lcd[$10] and $04);  { 0e }
    View7seg (148, 14, 153, 16, lcd[$10] and $08); { 0d }
    ViewHlp (112, 9, 123, 49, 31, lcd[$10] and $10); { TURBO }
  end {if};
{$ENDIF}

  if Scr[$18] <> lcd[$18] then
  begin
    Scr[$18] := lcd[$18];
    ViewHlp (71, 5, 84, 56, 17, lcd[$18] and $01);	{ RAD }
  end {if};

  if Scr[$20] <> lcd[$20] then
  begin
    Scr[$20] := lcd[$20];
    ViewHlp (93, 5, 102, 56, 17, lcd[$20] and $01);	{ GRA }
  end {if};

  if Scr[$28] <> lcd[$28] then
  begin
    Scr[$28] := lcd[$28];
{$IFDEF MK85MM}
    ViewHlp (112, 0, 123, 42, 29, lcd[$28] and $01); { TRACE }
    View7seg (153, 10, 155, 15, lcd[$28] and $02);      { 0c }
    View7seg (154, 4, 156, 9, lcd[$28] and $04);        { 0b }
    View7seg (150, 2, 155, 4, lcd[$28] and $08);        { 0a }
    View7seg (137, 8, 142, 10, lcd[$28] and $10); { minus }
{$ELSE}
    ViewHlp (116, 5, 120, 56, 11, lcd[$28] and $01); { TR }
{$ENDIF}
  end {if};

  if Scr[$30] <> lcd[$30] then
  begin
    Scr[$30] := lcd[$30];
    View7seg (142+o1, 3, 144+o1, 8, lcd[$30] and $01);   { 1f }
    View7seg (143+o1, 8, 148+o1, 10, lcd[$30] and $02);  { 1g }
    View7seg (141+o1, 9, 143+o1, 14, lcd[$30] and $04);  { 1e }
    View7seg (142+o1, 14, 147+o1, 16, lcd[$30] and $08); { 1d }
    View7seg (147+o1, 10, 149+o1, 15, lcd[$30] and $10); { 1c }
  end {if};

  if Scr[$38] <> lcd[$38] then
  begin
    Scr[$38] := lcd[$38];
    View7seg (148+o1, 4, 150+o1, 9, lcd[$38] and $01);	 { 1b }
    View7seg (144+o1, 2, 149+o1, 4, lcd[$38] and $02);	 { 1a }
    View7seg (158+o2, 3, 160+o2, 8, lcd[$38] and $04);	 { 2f }
    View7seg (159+o2, 8, 164+o2, 10, lcd[$38] and $08);	 { 2g }
    View7seg (157+o2, 9, 159+o2, 14, lcd[$38] and $10);	 { 2e }
  end {if};

  if Scr[$40] <> lcd[$40] then
  begin
    Scr[$40] := lcd[$40];
    View7seg (158+o2, 14, 163+o2, 16, lcd[$40] and $01); { 2d }
    View7seg (163+o2, 10, 165+o2, 15, lcd[$40] and $02); { 2c }
    View7seg (164+o2, 4, 166+o2, 9, lcd[$40] and $04);	 { 2b }
    View7seg (160+o2, 2, 165+o2, 4, lcd[$40] and $08);	 { 2a }
    View7seg (174+o3, 3, 176+o3, 8, lcd[$40] and $10);	 { 3f }
  end {if};

  if Scr[$48] <> lcd[$48] then
  begin
    Scr[$48] := lcd[$48];
    View7seg (175+o3, 8, 180+o3, 10, lcd[$48] and $01);  { 3g }
    View7seg (173+o3, 9, 175+o3, 14, lcd[$48] and $02);  { 3e }
    View7seg (174+o3, 14, 179+o3, 16, lcd[$48] and $04); { 3d }
    View7seg (179+o3, 10, 181+o3, 15, lcd[$48] and $08); { 3c }
    View7seg (180+o3, 4, 182+o3, 9, lcd[$48] and $10);   { 3b }
  end {if};

  if Scr[$50] <> lcd[$50] then
  begin
    Scr[$50] := lcd[$50];
    View7seg (176+o3, 2, 181+o3, 4, lcd[$50] and $01);   { 3a }
    View7seg (190+o4, 3, 192+o4, 8, lcd[$50] and $02);   { 4f }
    View7seg (191+o4, 8, 196+o4, 10, lcd[$50] and $04);  { 4g }
    View7seg (189+o4, 9, 191+o4, 14, lcd[$50] and $08);  { 4e }
    View7seg (190+o4, 14, 195+o4, 16, lcd[$50] and $10); { 4d }
  end {if};

  if Scr[$58] <> lcd[$58] then
  begin
    Scr[$58] := lcd[$58];
    View7seg (195+o4, 10, 197+o4, 15, lcd[$58] and $01); { 4c }
    View7seg (196+o4, 4, 198+o4, 9, lcd[$58] and $02);   { 4b }
    View7seg (192+o4, 2, 197+o4, 4, lcd[$58] and $04);   { 4a }
{$IFDEF MK85MM}
    ViewHlp (204, 0, 132, 56, 23, lcd[$58] and $08);   { STOP }
    ViewHlp (212, 9, 122, 56, 15, lcd[$58] and $10);    { INS }
{$ELSE}
    ViewHlp (204, 5, 132, 56, 23, lcd[$58] and $08);	{ STOP }
{$ENDIF}
  end {if};

end; {proc View}


{ In order to avoid display flickers all drawing is done off-screen
  on Face.Canvas, then periodically transferred to LcdImage }
procedure TMainForm.OnRefreshTimer(Sender: TObject);
begin
  View;
  if RedrawReq = True then LcdImage.Picture.Bitmap := LcdBmp;
  RedrawReq := False;
end;


{ release a pressed key if it's placed outside the coordinates X,Y }
procedure ReleaseKey1 (X, Y: Integer);
var
  i, r, c, k: integer;
begin
  if KeyCode1 = 0 then Exit;

{ locate the "keyblock" the key "KeyCode1" belongs to }
  i := 0;	{ "keyblock" index }
  k := 1;	{ first key code in the "keyblock" }
  while (KeyCode1 >= k + keypad[i].cnt) and (i < KEYPADS) do
  begin
    Inc (k, keypad[i].cnt);
    Inc (i);
  end {while};

  with keypad[i] do
  begin
    k := KeyCode1 - k;		{ offset of the key in the "keyblock" }
    c := L + SX*(k mod col);	{ X coordinate of the key image }
    r := T + SY*(k div col);	{ Y coordinate of the key image } 
    if (X < c) or (X >= c + W) or (Y < r) or (Y >= r + H) then
    begin
{ shift the key label up-left to get an impression of a released key }
      if KeyCode1 >= 2 then	{ power switch excluded }
      begin
        BitMap.Width := W-9;
        if KeyCode1 >= 37 then BitMap.Height := H-9
          else BitMap.Height := H-8;
        BitMap.Transparent := False;
        BitMap.Canvas.Draw (-c-5, -r-5, Face);
        Face.Canvas.Draw (c+4, r+4, BitMap);
      end {if};
      DrawKey (i, c, r, False);
      KeyCode1 := 0;
    end {if};
  end {with};
end {ReleaseKey1};


{ called when mouse button pressed }
procedure TMainForm.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  i, r, c, k: Integer;
begin
{ proceed only when left mouse button pressed }
  if Button <> mbLeft then Exit;

  ReleaseKey1 (-1, -1);
  KeyCode1 := 1;
  for i := 0 to KEYPADS do
  begin
    with keypad[i] do
    begin
      if (X >= L) and (X < L+SX*col) and (((X-L) mod SX) < W) and
	(Y >= T) and (((Y-T) mod SY) < H) then
      begin
        c := (X-L) div SX;
        r := (Y-T) div SY;
        k := col*r + c;
        if k < cnt then
        begin
          Inc (KeyCode1, k);
          c := L+c*SX;
          r := T+r*SY;
{ shift the key label down-right to get an impression of a pressed key }
          if KeyCode1 >= 2 then		{ power switch excluded }
          begin
            BitMap.Width := W-9;
            if KeyCode1 >= 37 then BitMap.Height := H-9
              else BitMap.Height := H-8;
            BitMap.Transparent := False;
            BitMap.Canvas.Draw (-c-4, -r-4, Face);
            Face.Canvas.Draw (c+5, r+5, BitMap);
          end {if};
          DrawKey (i, c, r, True);
          break;
        end {if};
      end {if};
      Inc (KeyCode1, cnt);
    end {with};
  end {for};

  if KeyCode1 > LASTKEYCODE then	{ no valid key pressed }
  begin
    KeyCode1 := 0;
{ dragging a captionless form by clicking anywhere on the client area outside
  the controls }
    XPos := X;
    YPos := Y;
    MD := BorderStyle = bsNone;
  end {if};
end {proc};


{ called when mouse button released }
procedure TMainForm.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  K: integer;
begin
{ proceed only when left mouse button was pressed }
  if Button <> mbLeft then Exit;

  MD := False;
  K := KeyCode1;
{ release a pressed key }
  ReleaseKey1 (-1, -1);

{ what to do if the mouse button was released over a pressed ... }
  case K of
    1: Close;				{ ...power switch }
    39: if (kbdrows and 2) <> 0 then	{ ...STOP key when row 1 selected }
		HALT_i := true;
  end {case};
end;


{ called when moving the mouse while the button pressed }
procedure TMainForm.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if MD then	{ drag the face image }
    Invalidate
  else		{ release a pressed key if mouse was moved from it }
    ReleaseKey1 (X, Y);
end;


procedure TMainForm.FormShow(Sender: TObject);
var
  X: Integer;
begin
  KeyCode1 := 0;
  KeyCode2 := 0;
  CpuStop := False;
  CpuDelay := 0;
  CpuSteps := -1;
  BreakPoint := -1;
{ load the Keys.bmp image }
  if FileExists (KeysName) then
    KeyBmp.LoadFromFile (KeysName)
  else
    MessageDlg (LoadMsg + KeysName, mtWarning, [mbOk], 0);
  KeyBmp.Transparent := False;
{ load the Overlay.bmp image }
  if FileExists (OverlayName) then
    OverlayBmp.LoadFromFile (OverlayName)
  else
    MessageDlg (LoadMsg + OverlayName, mtWarning, [mbOk], 0);
  OverlayBmp.Transparent := False;
{ draw the background image on the Face.Canvas }
  if FileExists (FaceName) then
  begin
    BitMap.LoadFromFile (FaceName);
    BitMap.Transparent := False;
    Face.Canvas.Draw (0, 0, BitMap);
{ select the calculator name MK85/MK85M depending on the RAM size }
    if RamSize <= MINRAMSIZE then	{ shift the MK85 emblem right }
    begin
      BitMap.Width := 100;
      BitMap.Height := 15;
      BitMap.Canvas.Draw (-616, -50, Face);
      Face.Canvas.Draw (639, 50, BitMap);
    end {if};
  end
  else
    MessageDlg (LoadMsg + FaceName, mtWarning, [mbOk], 0);
  Face.Transparent := False;
{ clear the LCD area }
  with LcdBmp.Canvas do
  begin
    Brush.Style := bsSolid;
{$IFDEF MK85MM}
    Brush.Color := clBlack;
{$ELSE}
    Brush.Color := clWhite;
{$ENDIF}
    FillRect (Rect(0, 0, 227, 51));
  end {with};
{ clear the display memory }
  for X := 0 to 95 do
  begin
    Scr[X] := $FF;
    lcd[X] := $00;
  end {for};
  CpuReset;
  RunTimer.Enabled := True;
  RefreshTimer.Enabled := True;
  CursorTimer.Enabled := True;
  RedrawReq := True;
end;


{ load the ROM and RAM images }
procedure MemLoad;
var
  f: file;
begin
{ load the ROM image }
  if FileExists (RomName) then
  begin
    AssignFile (f, RomName);
    Reset (f, 1);
    BlockRead (f, rom, ROMSIZE);
    CloseFile (f);
  end
  else MessageDlg (LoadMsg + RomName, mtWarning, [mbOk], 0);
{ load the RAM image }
  if FileExists (RamName) then
  begin
    AssignFile (f, RamName);
    Reset (f, 1);
    BlockRead (f, ram, RamSize);
    CloseFile (f);
  end;
end {MemLoad};


procedure IniLoad;
var
  IniMK: TIniFile;
begin
  IniMK := TIniFile.Create (ExpandFileName(IniName));
  with IniMK do
  begin
    CpuSpeed := ReadInteger ('Settings', 'CpuSpeed', 250);
    RamSize := ReadInteger ('Settings', 'RamSize', 6144);
  end {with};
  IniMK.Free;
end {IniLoad};


{ initialise the application }
procedure TMainForm.FormCreate(Sender: TObject);
begin
  BitMap := TBitMap.Create;
  Face := TBitMap.Create;
  Face.Width := 760;
  Face.Height := 330;
  LcdBmp := TBitMap.Create;
  LcdBmp.Width := 227;
  LcdBmp.Height := 51;
  KeyBmp := TBitMap.Create;
  KeyBmp.Width := 188;
  KeyBmp.Height := 63;
  OverlayBmp := TBitMap.Create;
  OverlayBmp.Width := 505;
  OverlayBmp.Height := 24;
  IniLoad;
  if RamSize < MINRAMSIZE then RamSize := MINRAMSIZE;
  if RamSize > MAXRAMSIZE then RamSize := MAXRAMSIZE;
  RamSize := (RamSize + 15) and $FFF0;
  RamEnd := RAMSTART + RamSize - 1;
  MemLoad;
end;


{ save the RAM image to the file Ram.bin }
procedure MemSave;
var
  f: file;
begin
  {$I-}
  AssignFile (f, RamName);
  Rewrite (f, 1);
  BlockWrite (f, ram, RamSize);
  CloseFile (f);
  {$I+}
  if IOResult <> 0 then MessageDlg (SaveMsg + RamName, mtWarning, [mbOk], 0);
end {MemSave};


{ terminate the application }
procedure TMainForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CpuStop := True;
  RunTimer.Enabled := False;
  RefreshTimer.Enabled := False;
  CursorTimer.Enabled := False;
  MemSave;
  BitMap.Free;
  Face.Free;
  LcdBmp.Free;
  KeyBmp.Free;
  OverlayBmp.Free;
end;


procedure TMainForm.OnCursorTimer(Sender: TObject);
begin
  CurVis := not CurVis;
end;


{ show/hide the keyboard overlay }
procedure OverlayFlip;
var
  Temp: TBitMap;
  i, y, r: integer;
begin
  Temp := TBitMap.Create;
  Temp.Width := 505;
  Temp.Height := 8;
  Temp.Transparent := False;
  y := 0;
  r := 190;
  for i := 0 to 2 do
  begin
{$IFDEF MK85MM}
    BitMap.Clear;
{$ENDIF}
    BitMap.Width := 505;
    BitMap.Height := 8;
    BitMap.Transparent := False;
    Temp.Canvas.Draw (-5, -r, Face);
    BitMap.Canvas.Draw (0, -y, OverlayBmp);
    OverlayBmp.Canvas.Draw (0, y, Temp);
    BitMap.TransparentColor := $0000FF00;
    BitMap.Transparent := True;
    Face.Canvas.Draw (5, r, BitMap);
    Inc (y, 8);
    Inc (r, 42);
  end {for};
  Temp.Free;
  MainForm.Invalidate;
end {OverlayFlip};


procedure TMainForm.FormKeyPress(Sender: TObject; var Key: Char);
const
{ key codes 7 to 54 }
  Letters: string[48] = 'QWERTYUIOPASDFGHJKLaZXCVBNM =aaaa/789*456-123+0.';
var
  i: integer;
begin
  i := 1;
  Key := UpCase(Key);
  while (i <= 48) and (Key <> Letters[i]) do Inc (i);
  if i <= 48 then KeyCode2 := i+6;
end;


procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_INSERT:	KeyCode2 := 2;	{ MODE }
    VK_LEFT:	KeyCode2 := 3;	{ <- }
    VK_RIGHT:	KeyCode2 := 4;	{ -> }
    VK_HOME:	KeyCode2 := 5;	{ [S] }
    VK_END:	KeyCode2 := 6;	{ [F] }
    VK_ESCAPE:	KeyCode2 := 37;	{ AC }
    VK_DELETE:	KeyCode2 := 38;	{ DEL }
    VK_RETURN:  KeyCode2 := 55;	{ EXE }
    VK_F2:	OverlayFlip;
    VK_F3:	DebugForm.Show;
    VK_F8:	KeyCode2 := 56;	{ Init }
  end {case};
end;


procedure TMainForm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyCode2 := 0;
end;


procedure TMainForm.FormPaint(Sender: TObject);
begin
  if MD then with Mouse.CursorPos do SetBounds(X-XPos, Y-YPos, Width, Height);
  Face.Canvas.Draw (70, 41, LcdBmp);
  Canvas.Draw (0, 0, Face);
end;


{ execute a bunch of machine code instructions }
procedure TMainForm.OnRunTimer(Sender: TObject);
var
  i: integer;
begin
  if CpuDelay > 0 then
  begin
    Dec (CpuDelay);
    Exit;
  end {if};
  i := 0;
  while i < CpuSpeed do
  begin
    if CpuStop then exit;
    Inc (i, CpuRun);
    if CpuSteps > 0 then
    begin
      Dec (CpuSteps);
      if CpuSteps = 0 then
      begin
        DebugForm.Show;
        break;
      end {if};
    end {if};
    if (BreakPoint >= 0) and (BreakPoint = ptrw(@reg[R7])^) then
    begin
      DebugForm.Show;
      break;
    end {if};
  end {while};
end;


procedure TMainForm.FormDeactivate(Sender: TObject);
begin
  ReleaseKey1 (-1, -1);
  KeyCode2 := 0;
end;


procedure TMainForm.ApplicationDeactivate(Sender: TObject);
begin
  ReleaseKey1 (-1, -1);
  KeyCode2 := 0;
end;

end.
