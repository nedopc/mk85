object MainForm: TMainForm
  Left = 259
  Height = 330
  Top = 709
  Width = 760
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsNone
  Caption = 'MK85'
  ClientHeight = 330
  ClientWidth = 760
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  KeyPreview = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnKeyUp = FormKeyUp
  OnMouseDown = FormMouseDown
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  OnPaint = FormPaint
  OnShow = FormShow
  Position = poScreenCenter
  LCLVersion = '1.6.2.0'
  Visible = True
  object LcdImage: TImage
    Left = 70
    Height = 51
    Top = 41
    Width = 227
  end
  object RefreshTimer: TTimer
    Enabled = False
    Interval = 32
    OnTimer = OnRefreshTimer
    left = 32
    top = 8
  end
  object CursorTimer: TTimer
    Enabled = False
    Interval = 400
    OnTimer = OnCursorTimer
    left = 32
    top = 64
  end
  object RunTimer: TTimer
    Interval = 10
    OnTimer = OnRunTimer
    left = 32
    top = 120
  end
end
