object DebugForm: TDebugForm
  Left = 217
  Height = 373
  Top = 283
  Width = 625
  Caption = 'Debug Window'
  ClientHeight = 373
  ClientWidth = 625
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpFixed
  OnCreate = DebugCreate
  OnHide = DebugHide
  OnShow = DebugShow
  Position = poScreenCenter
  LCLVersion = '1.6.0.4'
  object BinPanel: TPanel
    Left = 8
    Height = 161
    Top = 176
    Width = 601
    Anchors = [akTop, akLeft, akBottom]
    BorderStyle = bsSingle
    ClientHeight = 159
    ClientWidth = 599
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Pitch = fpFixed
    ParentFont = False
    TabOrder = 1
    OnClick = BinPanelClick
    object BinPaintBox: TPaintBox
      Left = 8
      Height = 135
      Top = 16
      Width = 559
      Anchors = [akTop, akLeft, akRight, akBottom]
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Pitch = fpFixed
      ParentFont = False
      OnMouseDown = BinPaintBoxMouseDown
      OnPaint = BinPaintBoxPaint
    end
    object BinLabel: TLabel
      Left = 8
      Height = 13
      Top = 1
      Width = 74
      Caption = 'Binary Editor'
      ParentColor = False
    end
    object BinScrollBar: TScrollBar
      Left = 577
      Height = 135
      Top = 16
      Width = 14
      Anchors = [akTop, akRight, akBottom]
      Kind = sbVertical
      PageSize = 0
      TabOrder = 1
      TabStop = False
      OnScroll = BinBoxScroll
    end
    object RadioButtonByte: TRadioButton
      Tag = 1
      Left = 104
      Height = 22
      Top = -1
      Width = 48
      Caption = 'byte'
      Checked = True
      OnClick = BinRadioButtonClick
      TabOrder = 2
      TabStop = True
    end
    object RadioButtonWord: TRadioButton
      Tag = 2
      Left = 160
      Height = 22
      Top = -1
      Width = 52
      Caption = 'word'
      OnClick = BinRadioButtonClick
      TabOrder = 3
    end
    object BinEdit: TEdit
      Left = 32
      Height = 19
      Top = 32
      Width = 40
      BorderStyle = bsNone
      Color = clYellow
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Pitch = fpFixed
      MaxLength = 4
      OnChange = BinEditChange
      OnKeyDown = BinEditKeyDown
      ParentFont = False
      TabStop = False
      TabOrder = 0
    end
  end
  object ListPanel: TPanel
    Left = 8
    Height = 161
    Top = 8
    Width = 313
    BorderStyle = bsSingle
    ClientHeight = 159
    ClientWidth = 311
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Pitch = fpFixed
    ParentFont = False
    TabOrder = 2
    OnClick = ListPanelClick
    object ListPaintBox: TPaintBox
      Left = 8
      Height = 135
      Top = 16
      Width = 271
      Anchors = [akTop, akLeft, akRight, akBottom]
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Pitch = fpFixed
      ParentFont = False
      OnMouseDown = ListPaintBoxMouseDown
      OnPaint = ListPaintBoxPaint
    end
    object ListLabel: TLabel
      Left = 8
      Height = 13
      Top = 1
      Width = 71
      Caption = 'Disassembly'
      ParentColor = False
    end
    object ListScrollBar: TScrollBar
      Left = 288
      Height = 135
      Top = 16
      Width = 14
      Anchors = [akTop, akRight, akBottom]
      Kind = sbVertical
      PageSize = 0
      TabOrder = 1
      TabStop = False
      OnScroll = ListBoxScroll
    end
    object ListEdit: TEdit
      Left = 32
      Height = 19
      Top = 32
      Width = 40
      BorderStyle = bsNone
      Color = clYellow
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Pitch = fpFixed
      MaxLength = 4
      OnChange = ListEditChange
      OnKeyDown = ListEditKeyDown
      ParentFont = False
      TabStop = False
      TabOrder = 0
    end
  end
  object RegPanel: TPanel
    Left = 328
    Height = 161
    Top = 8
    Width = 121
    BorderStyle = bsSingle
    ClientHeight = 159
    ClientWidth = 119
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Pitch = fpFixed
    ParentFont = False
    TabOrder = 3
    OnClick = RegPanelClick
    object RegPaintBox: TPaintBox
      Left = 8
      Height = 135
      Top = 16
      Width = 79
      Anchors = [akTop, akLeft, akRight, akBottom]
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Pitch = fpFixed
      ParentFont = False
      OnMouseDown = RegPaintBoxMouseDown
      OnPaint = RegPaintBoxPaint
    end
    object RegLabel: TLabel
      Left = 8
      Height = 13
      Top = 1
      Width = 53
      Caption = 'Registers'
      ParentColor = False
    end
    object RegScrollBar: TScrollBar
      Left = 97
      Height = 135
      Top = 16
      Width = 14
      Anchors = [akTop, akRight, akBottom]
      Kind = sbVertical
      Max = 0
      PageSize = 0
      TabOrder = 1
      TabStop = False
      OnScroll = RegBoxScroll
    end
    object RegEdit: TEdit
      Left = 32
      Height = 19
      Top = 32
      Width = 40
      BorderStyle = bsNone
      CharCase = ecUppercase
      Color = clYellow
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Pitch = fpFixed
      MaxLength = 4
      OnChange = RegEditChange
      OnKeyDown = RegEditKeyDown
      ParentFont = False
      TabStop = False
      TabOrder = 0
    end
  end
  object StepPanel: TPanel
    Left = 456
    Height = 51
    Top = 8
    Width = 153
    BorderStyle = bsSingle
    ClientHeight = 49
    ClientWidth = 151
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Pitch = fpFixed
    ParentFont = False
    TabOrder = 4
    OnClick = StepPanelClick
    object StepLabel: TLabel
      Left = 8
      Height = 13
      Top = 1
      Width = 63
      Caption = 'Single Step'
      ParentColor = False
    end
    object StepButton: TButton
      Left = 88
      Height = 25
      Top = 18
      Width = 49
      Caption = 'Run'
      OnClick = StepButtonClick
      TabOrder = 0
      TabStop = False
    end
  end
  object TracePanel: TPanel
    Left = 456
    Height = 51
    Top = 63
    Width = 153
    BorderStyle = bsSingle
    ClientHeight = 49
    ClientWidth = 151
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Pitch = fpFixed
    ParentFont = False
    TabOrder = 5
    OnClick = TracePanelClick
    object TraceLabel: TLabel
      Left = 8
      Height = 13
      Top = 1
      Width = 94
      Caption = 'Number of steps'
      ParentColor = False
    end
    object TraceEdit: TEdit
      Left = 16
      Height = 25
      Top = 18
      Width = 57
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Pitch = fpFixed
      MaxLength = 6
      OnChange = TraceEditChange
      OnClick = TracePanelClick
      ParentFont = False
      TabStop = False
      TabOrder = 1
    end
    object TraceButton: TButton
      Left = 88
      Height = 25
      Top = 18
      Width = 49
      Caption = 'Run'
      OnClick = TraceButtonClick
      TabOrder = 0
      TabStop = False
    end
  end
  object BpPanel: TPanel
    Left = 456
    Height = 51
    Top = 118
    Width = 153
    BorderStyle = bsSingle
    ClientHeight = 49
    ClientWidth = 151
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Pitch = fpFixed
    ParentFont = False
    TabOrder = 0
    OnClick = BpPanelClick
    object BpLabel: TLabel
      Left = 8
      Height = 13
      Top = 1
      Width = 111
      Caption = 'Breakpoint Address'
      ParentColor = False
    end
    object BpEdit: TEdit
      Left = 16
      Height = 25
      Top = 18
      Width = 57
      CharCase = ecUppercase
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Pitch = fpFixed
      OnChange = BpEditChange
      OnClick = BpPanelClick
      ParentFont = False
      TabStop = False
      TabOrder = 1
    end
    object BpButton: TButton
      Left = 88
      Height = 25
      Top = 18
      Width = 49
      Caption = 'Run'
      OnClick = BpButtonClick
      TabOrder = 0
      TabStop = False
    end
  end
end
